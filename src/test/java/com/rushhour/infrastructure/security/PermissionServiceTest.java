package com.rushhour.infrastructure.security;

import com.rushhour.domain.activity.service.ActivityService;
import com.rushhour.domain.appointment.service.AppointmentService;
import com.rushhour.domain.client.service.ClientService;
import com.rushhour.domain.employee.service.EmployeeService;
import com.rushhour.domain.provider.service.ProviderService;
import com.rushhour.domain.role.service.RoleService;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ContextConfiguration(classes = {PermissionService.class})
@ExtendWith(SpringExtension.class)
class PermissionServiceTest {
    @MockBean
    private ActivityService activityService;

    @MockBean
    private AppointmentService appointmentService;

    @MockBean
    private ClientService clientService;

    @MockBean
    private EmployeeService employeeService;

    @Autowired
    private PermissionService permissionService;

    @MockBean
    private ProviderService providerService;

    @MockBean
    private RoleService roleService;

    /**
     * Method under test: {@link PermissionService#canClientAccessAppointment(Long)}
     */
    @Test
    @Disabled("TODO: Complete this test")
    void testCanClientAccessAppointment() {
        // TODO: Complete this test.
        //   Reason: R013 No inputs found that don't throw a trivial exception.
        //   Diffblue Cover tried to run the arrange/act section, but the method under
        //   test threw
        //   java.lang.NullPointerException: Cannot invoke "org.springframework.security.core.Authentication.getPrincipal()" because the return value of "org.springframework.security.core.context.SecurityContext.getAuthentication()" is null
        //       at com.rushhour.infrastructure.security.PermissionService.getPrincipal(PermissionService.java:196)
        //       at com.rushhour.infrastructure.security.PermissionService.canClientAccessAppointment(PermissionService.java:142)
        //   See https://diff.blue/R013 to resolve this issue.

        permissionService.canClientAccessAppointment(123L);
    }
}

