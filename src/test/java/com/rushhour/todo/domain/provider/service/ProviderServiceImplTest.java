package com.rushhour.todo.domain.provider.service;

import com.rushhour.domain.provider.entity.Provider;
import com.rushhour.domain.provider.model.ProviderRequestDTO;
import com.rushhour.domain.provider.model.ProviderResponseDTO;
import com.rushhour.domain.provider.model.ProviderUpdateDTO;
import com.rushhour.domain.provider.repository.ProviderRepository;
import com.rushhour.domain.provider.service.ProviderServiceImpl;
import com.rushhour.infrastructure.handler.ConflictException;
import com.rushhour.infrastructure.handler.NotFoundException;
import com.rushhour.infrastructure.mappers.ProviderMapperImpl;
import org.assertj.core.api.AssertionsForClassTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.*;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.mockito.ArgumentMatchers.any;

@ExtendWith(MockitoExtension.class)
class ProviderServiceImplTest {
    Provider provider;

    Page<Provider> providers;

    @InjectMocks
    ProviderServiceImpl providerService;

    @Mock
    ProviderRepository providerRepository;

    @Mock
    ProviderMapperImpl providerMapper;

    @BeforeEach
    void setUp() {
        provider = new Provider();
        provider.setName("Aleksa");
        provider.setWebsite("https://www.aleksa.com");
        provider.setBusinessDomain("aleksa");
        provider.setPhone("+604050354");
        provider.setStartTimeOfWorkingDays(LocalTime.of(9, 0, 0));
        provider.setEndTimeOfWorkingDays(LocalTime.of(17, 0, 0));
        provider.setWorkingDays(new HashSet(Arrays.asList(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY)));

        List<Provider> providerList = new ArrayList<>();
        providerList.add(provider);
        providers = new PageImpl<>(providerList);
    }

    @Test
    void testInvalidProviderAccountByName() {
        ProviderRequestDTO providerDTO = new ProviderRequestDTO( "Aleksa", "https://www.aleksa.com", "as", "+604050276", LocalTime.of(9, 0, 0), LocalTime.of(17, 0, 0),new HashSet<>(Set.of(DayOfWeek.MONDAY)));
        Mockito.when(providerRepository.existsProviderByName("Aleksa")).thenReturn(true);
        Assertions.assertThrows(ConflictException.class, () -> providerService.create(providerDTO));
    }

    @Test
    void testInvalidProviderAccountByBusinessDomain() {
        ProviderRequestDTO providerDTO = new ProviderRequestDTO("Aleksa", "https://www.aleksa.com", "as", "+604050276", LocalTime.of(9, 0, 0), LocalTime.of(17, 0, 0), new HashSet<>(Set.of(DayOfWeek.MONDAY)));
        Mockito.when(providerRepository.existsProviderByBusinessDomain("as")).thenReturn(true);
        Assertions.assertThrows(ConflictException.class, () -> providerService.create(providerDTO));
    }

    @Test
    void testValidProviderAccount() {
        ProviderRequestDTO providerDTO = new ProviderRequestDTO( "Aleksa", "https://www.aleksa.com", "as", "+604050276", LocalTime.of(9, 0,0), LocalTime.of(17, 0,0),new HashSet<>(Set.of(DayOfWeek.MONDAY)));
        ProviderResponseDTO providerResponseDTO = new ProviderResponseDTO(1L, "Aleksa", "https://www.aleksa.com", "as", "+604050276", LocalTime.of(9, 0,0), LocalTime.of(17, 0,0), new HashSet<>(Set.of(DayOfWeek.MONDAY)));

        Mockito.when(providerRepository.existsProviderByName("Aleksa")).thenReturn(false);
        Mockito.when(providerRepository.existsProviderByBusinessDomain("as")).thenReturn(false);

        Mockito.when(providerRepository.save(any())).thenReturn(provider);
        Mockito.when(providerMapper.toProviderDto(any())).thenReturn(providerResponseDTO);

        ProviderResponseDTO createdProvider = providerService.create(providerDTO);
        assertThat(createdProvider).usingRecursiveComparison().isEqualTo(providerResponseDTO);

    }

    @Test
    void testGetById() {
        ProviderResponseDTO providerDTO = new ProviderResponseDTO(1L, "Aleksa", "https://aleksa.com", "as", "+604050276", LocalTime.of(9,0,0), LocalTime.of(17,0,0), new HashSet<>(Set.of(DayOfWeek.MONDAY)));

        Mockito.when(providerMapper.toProviderDto(any())).thenReturn(providerDTO);
        Mockito.when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));
        ProviderResponseDTO returned = providerService.getById(1L);

        AssertionsForClassTypes.assertThat(returned).usingRecursiveComparison().isEqualTo(providerDTO);
    }

    @Test
    void testInvalidGetById() {
        Mockito.when(providerRepository.findById(1L)).thenReturn(Optional.empty());
        Assertions.assertThrows(NotFoundException.class, () -> providerService.getById(1L));
    }

    @Test
    void testGetAll() {
        Pageable pageable = Mockito.mock(Pageable.class);

        ProviderResponseDTO providerDTO = new ProviderResponseDTO(1L, "Aleksa", "https://aleksa.com", "as", "+604050276", LocalTime.of(9,0,0), LocalTime.of(17,0,0),new HashSet<>(Set.of(DayOfWeek.MONDAY)));

        Mockito.when(providerMapper.toProviderDto(provider)).thenReturn(providerDTO);

        Page<ProviderResponseDTO> expectedProviders = providers.map(provider -> (providerMapper.toProviderDto(provider)));

        Mockito.when(providerRepository.findAll(pageable)).thenReturn(providers);

        Page<ProviderResponseDTO> returnedProviders = providerService.getAll(pageable);

        assertThat(expectedProviders).usingRecursiveComparison().isEqualTo(returnedProviders);

    }

    @Test
    void testUpdate() {
        ProviderUpdateDTO updateProviderDTO = new ProviderUpdateDTO("Aleksaaa", "https://www.aleksa.com", "aleksa", LocalTime.of(9,0,0), LocalTime.of(17,0,0), new HashSet<>(singletonList(DayOfWeek.MONDAY)));
        ProviderResponseDTO providerResponseDTO = new ProviderResponseDTO(1L, "Aleksaaa", "https://www.aleksa.com", "prime", "+01860304050", LocalTime.of(9,0,0),  LocalTime.of(17,0,0), new HashSet<>(singletonList(DayOfWeek.MONDAY)));

        Mockito.when(providerRepository.findById(1L)).thenReturn(Optional.of(provider));
        Mockito.when(providerRepository.save(provider)).thenReturn(provider);
        Mockito.doCallRealMethod().when(providerMapper).updateProviderFromDto(updateProviderDTO, provider);
        Mockito.when(providerMapper.toProviderDto(provider)).thenReturn(providerResponseDTO);

        ProviderResponseDTO providerDTO = providerService.update(1L, updateProviderDTO);

        assertThat(providerResponseDTO).usingRecursiveComparison().isEqualTo(providerDTO);
    }


    @Test
    void testInvalidUpdateById() {
        ProviderUpdateDTO providerDTO = new ProviderUpdateDTO ("Aleksa", "https://www.aleksa.com", "+604050276", LocalTime.of(9,0,0), LocalTime.of(17,0,0),new HashSet<>(Set.of(DayOfWeek.MONDAY)));
        Mockito.when(providerRepository.findById(2L)).thenReturn(Optional.empty());
        Assertions.assertThrows(NotFoundException.class, () -> providerService.update(2L, providerDTO));
    }

    @Test
    void testDeleteShouldThrownExceptionWhenProviderNotFound() {
        Mockito.when(providerRepository.existsById(2L)).thenReturn(false);
        Assertions.assertThrows(NotFoundException.class, () -> providerService.delete(2L));
    }

    @Test
    void testDeleteShouldDeleteWhenProviderFound() {
        Mockito.when(providerRepository.existsById(1L)).thenReturn(true);
        Mockito.doNothing().when(providerRepository).deleteById(1L);
        Assertions.assertDoesNotThrow(() -> providerService.delete(1L));
    }
}
