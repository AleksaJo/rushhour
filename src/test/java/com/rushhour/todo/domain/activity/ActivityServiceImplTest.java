package com.rushhour.todo.domain.activity;

import com.rushhour.domain.activity.entity.Activity;
import com.rushhour.domain.activity.model.ActivityRequestDTO;
import com.rushhour.domain.activity.model.ActivityResponseDTO;
import com.rushhour.domain.activity.model.ActivityUpdateDTO;
import com.rushhour.domain.activity.repository.ActivityRepository;
import com.rushhour.domain.activity.service.ActivityServiceImpl;
import com.rushhour.domain.employee.service.EmployeeService;
import com.rushhour.domain.provider.entity.Provider;
import com.rushhour.domain.provider.service.ProviderService;
import com.rushhour.infrastructure.handler.NotFoundException;
import com.rushhour.infrastructure.mappers.ActivityMapperImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ActivityServiceImplTest {
    Provider provider;

    Activity activity;

    @Mock
    ActivityMapperImpl activityMapper;

    @Mock
    ActivityRepository activityRepository;

    @InjectMocks
    ActivityServiceImpl activityService;

    @Mock
    ProviderService providerService;

    @Mock
    EmployeeService employeeService;

    @BeforeEach
    void setUp() {
        provider = new Provider();
        provider.setBusinessDomain("BusinessDomain");
        provider.setEmployees(new ArrayList<>());
        provider.setEndTimeOfWorkingDays(LocalTime.of(1, 1));
        provider.setId(1L);
        provider.setName("Name");
        provider.setPhone("0604050276");
        provider.setStartTimeOfWorkingDays(LocalTime.of(1, 1));
        provider.setWebsite("Prime");
        provider.setWorkingDays(new HashSet<>());

        activity = new Activity();
        activity.setDuration(null);
        activity.setEmployees(new ArrayList<>());
        activity.setId(1L);
        activity.setName("Aleksa");
        activity.setPrice(10.0d);
        activity.setProvider(provider);
    }

    @Test
    void create() {
        ActivityRequestDTO activityRequestDTO = new ActivityRequestDTO("Aleksa", 10.0d, null, activity.getProvider().getId(), List.of(1L));
        ActivityResponseDTO responseDTO = new ActivityResponseDTO(1L, "Aleksa", 10.0d, null, activity.getProvider().getId(), List.of(1L));

        when(activityMapper.toActivity(activityRequestDTO)).thenReturn(activity);
        when(providerService.getProviderById(provider.getId())).thenReturn(activity.getProvider());
        when(employeeService.getByIdsAndProviderId(List.of(1L), provider.getId())).thenReturn(new ArrayList<>());
        when(activityRepository.existsByNameAndProviderId(activityRequestDTO.name(), provider.getId())).thenReturn(false);
        when(activityRepository.save(activity)).thenReturn(activity);
        when(activityMapper.toResponseDTO(activity)).thenReturn(responseDTO);
        ActivityResponseDTO createdActivity = activityService.create(activityRequestDTO);
        verify(providerService).getProviderById(provider.getId());
        verify(activityRepository).existsByNameAndProviderId(activityRequestDTO.name(), provider.getId());
        assertThat(responseDTO).usingRecursiveComparison().isEqualTo(createdActivity);
    }

    @Test
    void getById() {
     ActivityResponseDTO responseDTO = new ActivityResponseDTO(1L, "Aleksa", 10.0d, null, activity.getProvider().getId(), List.of(1L));
     when(activityMapper.toResponseDTO(activity)).thenReturn(responseDTO);
     ActivityResponseDTO expectedActivity = activityMapper.toResponseDTO(activity);
     Mockito.doReturn(Optional.of(activity)).when(activityRepository).findById(1L);
     ActivityResponseDTO returnedActivity = activityService.getById(1L);
     Assertions.assertEquals(expectedActivity, returnedActivity);
     }

     @Test
     void testValidUpdate() {
         ActivityUpdateDTO updateDTO = new ActivityUpdateDTO("Aleksa", 10.0d, null, List.of(1L));

         ActivityResponseDTO responseDTO = new ActivityResponseDTO(1L, "Aleksa", 10.0d, null, activity.getProvider().getId(), List.of(1L));

         when(activityRepository.findById(1L)).thenReturn(Optional.of(activity));
         Mockito.doCallRealMethod().when(activityMapper).updateActivity(updateDTO, activity);
         when(activityRepository.save(activity)).thenReturn(activity);
         when(activityService.update(1L, updateDTO)).thenReturn(responseDTO);
         when(activityMapper.toResponseDTO(activity)).thenReturn(responseDTO);

         var updateResponseDto = activityService.update(1L, updateDTO);

         assertThat(responseDTO).usingRecursiveComparison().isEqualTo(updateResponseDto);

     }

     @Test
     void testUpdateNotFound() {
         ActivityUpdateDTO updateDTO = new ActivityUpdateDTO("Aleksa", 10.0d, null, List.of(1L));
         Mockito.doReturn(Optional.empty()).when(activityRepository).findById(1L);
         Assertions.assertThrows(NotFoundException.class, () -> activityService.update(1L, updateDTO));
     }

     @Test
     void testGetById_Not_Found() {
        Mockito.doReturn(Optional.empty()).when(activityRepository).findById(1L);
        Assertions.assertThrows(NotFoundException.class, () -> activityService.getById(1L));
     }

     @Test
     void testDelete_NotFound() {
        Mockito.doReturn(Optional.empty()).when(activityRepository).findById(1L);
        Assertions.assertThrows(NotFoundException.class, () -> activityService.delete(1L));
     }
}
