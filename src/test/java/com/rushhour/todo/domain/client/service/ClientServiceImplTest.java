package com.rushhour.todo.domain.client.service;

import com.rushhour.domain.account.entity.Account;
import com.rushhour.domain.account.model.AccountRequestDTO;
import com.rushhour.domain.account.model.AccountResponseDTO;
import com.rushhour.domain.account.model.AccountUpdateDTO;
import com.rushhour.domain.account.service.AccountService;
import com.rushhour.domain.client.entity.Client;
import com.rushhour.domain.client.model.ClientRequestDTO;
import com.rushhour.domain.client.model.ClientResponseDTO;
import com.rushhour.domain.client.model.ClientUpdateDTO;
import com.rushhour.domain.client.repository.ClientRepository;
import com.rushhour.domain.client.service.ClientServiceImpl;
import com.rushhour.domain.role.entity.Role;
import com.rushhour.domain.role.model.RoleDTO;
import com.rushhour.infrastructure.handler.NotFoundException;
import com.rushhour.infrastructure.mappers.ClientMapperImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class ClientServiceImplTest {

    Client client;
    Account account;
    Role role;
    Page<Client> clients;
    @InjectMocks
    ClientServiceImpl clientService;
    @Mock
    AccountService accountService;
    @Mock
    ClientRepository clientRepository;
    @Mock
    ClientMapperImpl clientMapper;

    @BeforeEach
    void setUp() {

        role = new Role();
        role.setName("CLIENT");
        role.setId(4L);

        account = new Account();
        account.setId(1L);
        account.setEmail("email@gmail.com");
        account.setFullName("test01");
        account.setPassword("password123$");
        account.setRole(role);

        client = new Client();
        client.setId(1L);
        client.setClientPhone("+604050276");
        client.setClientAddress("Cegarska");
        client.setAccount(account);

        List<Client> clientList = new ArrayList<>();
        clientList.add(client);
        clients = new PageImpl<>(clientList);

    }

    @Test
    void testCreateValid() {
        AccountRequestDTO accountRequestDTO = new AccountRequestDTO("email@gmail.com", "test01", "password123$", 4L);
        ClientRequestDTO clientDTO = new ClientRequestDTO("+604050276", "Cegarska", accountRequestDTO);

        AccountResponseDTO accountResponseDTO = new AccountResponseDTO(1L, "email@gmail.com", "test01", new RoleDTO(4L, "CLIENT"));
        ClientResponseDTO responseDTO = new ClientResponseDTO(1L, "+604050276", "Cegarska", accountResponseDTO);

        Mockito.when(clientMapper.toClient(clientDTO)).thenReturn(client);
        Mockito.when(clientRepository.save(client)).thenReturn(client);

        Mockito.when(clientMapper.toResponseDTO(client)).thenReturn(responseDTO);

        ClientResponseDTO createClient = clientService.create(clientDTO);

        assertThat(responseDTO).usingRecursiveComparison().isEqualTo(createClient);

    }

    @Test
    void testValidById() {
        ClientResponseDTO responseDTO = new ClientResponseDTO(1L, "+604050276", "Cegarska", null);
        Mockito.when(clientMapper.toResponseDTO(client)).thenReturn(responseDTO);

        ClientResponseDTO expectedClient = clientMapper.toResponseDTO(client);
        Mockito.doReturn(Optional.of(client)).when(clientRepository).findById(1L);
        ClientResponseDTO returnedClient = clientService.getById(1L);
        Assertions.assertEquals(expectedClient,returnedClient);
    }

    @Test
    void testGetByIdNotFound() {
        Mockito.doReturn(Optional.empty()).when(clientRepository).findById(1L);
        Assertions.assertThrows(NotFoundException.class, () -> clientService.getById(1L));
    }

    @Test
    void testUpdate() {
        AccountUpdateDTO accountUpdateDTO = new AccountUpdateDTO("email@gmail.com",  "password123$");
        ClientUpdateDTO clientUpdateDTO = new ClientUpdateDTO("+604050276", "Cegarska", accountUpdateDTO);

        AccountResponseDTO accountResponseDTO = new AccountResponseDTO(1L, "email@gmail.com", "test01", new RoleDTO(4L, "CLIENT"));
        ClientResponseDTO updateClient = new ClientResponseDTO(1L, "+604050276", "Cegarska", accountResponseDTO);

        Mockito.when(clientRepository.findById(1L)).thenReturn(Optional.of(client));
        Mockito.doCallRealMethod().when(clientMapper).updateClientFromDTO(clientUpdateDTO, client);
        Mockito.when(clientRepository.save(client)).thenReturn(client);
        Mockito.when(accountService.update(1L, accountUpdateDTO)).thenReturn(accountResponseDTO);
        Mockito.when(clientMapper.toResponseDTO(client)).thenReturn(updateClient);

        var updatedClient = clientService.update(1L, clientUpdateDTO);
        assertThat(updateClient).usingRecursiveComparison().isEqualTo(updatedClient);
    }

    @Test
    void testUpdateNotFound() {
        AccountUpdateDTO accountUpdateDTO = new AccountUpdateDTO("email@gmail.com",  "password123$");
        ClientUpdateDTO clientUpdateDTO = new ClientUpdateDTO("+604050276", "Cegarska", accountUpdateDTO);
        Mockito.doReturn(Optional.empty()).when(clientRepository).findById(1L);
        Assertions.assertThrows(NotFoundException.class, () -> clientService.update(1L, clientUpdateDTO));
    }

    @Test
    void testDeleteNotFound() {
        Mockito.doReturn(false).when(clientRepository).existsById(1L);
        Assertions.assertThrows(NotFoundException.class, () -> clientService.delete(1L));
    }
}
