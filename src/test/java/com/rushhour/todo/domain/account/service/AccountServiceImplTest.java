package com.rushhour.todo.domain.account.service;

import com.rushhour.domain.account.entity.Account;
import com.rushhour.domain.account.model.AccountRequestDTO;
import com.rushhour.domain.account.model.AccountResponseDTO;
import com.rushhour.domain.account.model.AccountUpdateDTO;
import com.rushhour.domain.account.repository.AccountRepository;
import com.rushhour.domain.account.service.AccountServiceImpl;
import com.rushhour.domain.role.entity.Role;
import com.rushhour.domain.role.enums.RoleNames;
import com.rushhour.domain.role.repository.RoleRepository;
import com.rushhour.infrastructure.handler.ConflictException;
import com.rushhour.infrastructure.handler.NotFoundException;
import com.rushhour.infrastructure.mappers.AccountMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {
    Account account;
    Role role;
    Page<Account> accounts;
    @InjectMocks
    AccountServiceImpl accountService;
    @Mock
    AccountMapper accountMapper;
    @Mock
    AccountRepository accountRepository;
    @Mock
    RoleRepository roleRepository;

    @BeforeEach
    void setUp() {
        account = new Account();
        account.setId(6L);
        account.setEmail("mihajlo.jovanovic.97@gmail.com");
        account.setFullName("Radovan");
        List<Account> accList = new ArrayList<>();
        accList.add(account);
        accounts = new PageImpl<>(accList);

        role = new Role();
        role.setId(1L);
        role.setName(RoleNames.CLIENT.name());
    }

    void compareAccountDTO(AccountResponseDTO expected, AccountResponseDTO returnedAccount) {
        Assertions.assertSame(expected.id(), returnedAccount.id());
        Assertions.assertSame(expected.email(), returnedAccount.email());
        Assertions.assertSame(expected.fullName(), returnedAccount.fullName());
    }

    @Test
    void testInvalidCreateAccount() {
        AccountRequestDTO accountDTO = new AccountRequestDTO("mihajlo.jovanovic.97@gmail.com","TestUser", "password123$", 14L);
        Mockito.when(accountRepository.existsAccountByEmail("mihajlo.jovanovic.97@gmail.com"))
                .thenReturn(true);
        Assertions.assertThrows(ConflictException.class, () -> accountService.create(accountDTO));
    }

    @Test
    void testValidCreateAccount() {
        AccountRequestDTO accountDTO = new AccountRequestDTO("mihajlo.jovanovic.97@gmail.com","TestUser", "password123$", 14L);

        Mockito.when(accountRepository.existsAccountByEmail("mihajlo.jovanovic.97@gmail.com"))
                .thenReturn(false);
        Mockito.when(accountRepository.save(any())).thenReturn(Mockito.mock(Account.class));

        Role mockedRole = new Role();
        mockedRole.setId(1L);
        mockedRole.setName("ROLE_ADMINISTRATOR");

        Mockito.when(roleRepository.findById(accountDTO.roleId())).thenReturn(Optional.of(mockedRole));

        Assertions.assertDoesNotThrow(() -> {
            accountService.create(accountDTO);
        });
    }

    @Test
    void testGetById() {
        AccountResponseDTO accountDTO = new AccountResponseDTO(6L, "mihajlo.jovanovic.97@gmail.com", "TestName", null);
        Mockito.when(accountMapper.toResponseDTO(account)).thenReturn(accountDTO);

        AccountResponseDTO expected = accountMapper.toResponseDTO(account);
        Mockito.doReturn(Optional.of(account)).when(accountRepository).findById(6L);
        AccountResponseDTO returnedAccount = accountService.getById(6L);

        compareAccountDTO(expected,returnedAccount);
    }

    @Test
    void testGetByInvalidId() {
        Mockito.doReturn(Optional.empty()).when(accountRepository).findById(6L);
        Assertions.assertThrows(NotFoundException.class, () -> accountService.getById(6L));
    }

    @Test
    void testGetAll() {
        Pageable pageable = Mockito.mock(Pageable.class);
        AccountResponseDTO accountDTO = new AccountResponseDTO(6L, "mihajlo.jovanovic.97@gmail.com", "TestName", null);

        Mockito.when(accountMapper.toResponseDTO(account)).thenReturn(accountDTO);

        Page<AccountResponseDTO> expectedAccounts = accounts.map((account) -> accountMapper.toResponseDTO(account));

        Mockito.doReturn(accounts).when(accountRepository).findAll(pageable);

        Page<AccountResponseDTO> accountPage = accountService.getAll(pageable);

        for (var i = 0; i < accountPage.getContent().size(); ++i) {
            compareAccountDTO(expectedAccounts.getContent().get(i), accountPage.getContent().get(i));
        }
    }

    @Test
    void testUpdate() {
        AccountUpdateDTO accountUpdateDTO = new AccountUpdateDTO("TestName", "password123$");
        AccountResponseDTO accountResponseDTO = new AccountResponseDTO(6L, "mihajlo.jovanovic.97@gmail.com", "TestName", null);

        Mockito.doReturn(Optional.of(account)).when(accountRepository).findById(6L);
        Mockito.doReturn(account).when(accountRepository).save(account);
        Mockito.when(accountMapper.toResponseDTO(account)).thenReturn(accountResponseDTO);
        AccountResponseDTO accountDTO = accountService.update(6L, accountUpdateDTO);

        Assertions.assertEquals(accountUpdateDTO.fullName(), accountDTO.fullName());
    }

    @Test
    void testUpdateWithInvalidId() {
        AccountUpdateDTO accountDTO = new AccountUpdateDTO("updated", "password123$");
        Mockito.doReturn(Optional.empty()).when(accountRepository).findById(5L);
        Assertions.assertThrows(NotFoundException.class, () -> accountService.update(5L, accountDTO));
    }

    @Test
    void testDeleteShouldThrowExceptionWhenAccountNotFound() {
        Mockito.doReturn(false).when(accountRepository).existsById(4L);
        Assertions.assertThrows(NotFoundException.class, () -> accountService.delete(4L));
    }

    @Test
    void testDeleteShouldDeleteAccountWhenFound() {
        Mockito.when(accountRepository.existsById(4L)).thenReturn(true);
        Mockito.doNothing().when(accountRepository).deleteById(4L);
        Assertions.assertDoesNotThrow(() -> accountService.delete(4L));
    }
}
