package com.rushhour.todo.domain.employee.service;

import com.rushhour.domain.account.entity.Account;
import com.rushhour.domain.account.model.AccountRequestDTO;
import com.rushhour.domain.account.model.AccountResponseDTO;
import com.rushhour.domain.account.model.AccountUpdateDTO;
import com.rushhour.domain.account.service.AccountServiceImpl;
import com.rushhour.domain.employee.entity.Employee;
import com.rushhour.domain.employee.model.EmployeeRequestDTO;
import com.rushhour.domain.employee.model.EmployeeResponseDTO;
import com.rushhour.domain.employee.model.EmployeeUpdateDTO;
import com.rushhour.domain.employee.repository.EmployeeRepository;
import com.rushhour.domain.employee.service.EmployeeServiceImpl;
import com.rushhour.domain.provider.entity.Provider;
import com.rushhour.domain.provider.service.ProviderService;
import com.rushhour.domain.role.entity.Role;
import com.rushhour.domain.role.model.RoleDTO;
import com.rushhour.infrastructure.handler.NotFoundException;
import com.rushhour.infrastructure.mappers.EmployeeMapperImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class EmployeeServiceImplTest {

    Employee employee;

    Account account;

    Provider provider;

    Role role;

    Page<Employee> employees;

    @InjectMocks
    EmployeeServiceImpl employeeService;

    @Mock
    AccountServiceImpl accountService;

    @Mock
    EmployeeRepository employeeRepository;

    @Mock
    EmployeeMapperImpl employeeMapper;

    @Mock
    ProviderService providerService;

    @BeforeEach
    void setUp() {

        role = new Role();
        role.setName("USER");
        role.setId(1L);

        account = new Account();
        account.setId(1L);
        account.setEmail("email@gmail.com");
        account.setFullName("test01");
        account.setPassword("password123$");
        account.setRole(role);

        provider = new Provider();
        provider.setId(1L);
        provider.setName("PrimeHolding");
        provider.setWebsite("https://www.aleksa2.com");
        provider.setBusinessDomain("prime");
        provider.setPhone("0182151545");
        provider.setStartTimeOfWorkingDays(LocalTime.of(9, 0, 0));
        provider.setEndTimeOfWorkingDays(LocalTime.of(17, 0, 0));
        provider.setWorkingDays(new HashSet<>(Set.of(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.WEDNESDAY, DayOfWeek.THURSDAY, DayOfWeek.FRIDAY)));

        employee = new Employee();
        employee.setId(1L);
        employee.setTitle("test");
        employee.setEmployeePhone("+38161141415");
        employee.setRatePerHour(2.0);
        employee.setHireDate(LocalDate.of(2000, 12, 12));
        employee.setAccount(account);

        List<Employee> employeeList = new ArrayList<>();
        employeeList.add(employee);
        employees = new PageImpl<>(employeeList);

    }

   @Test
    void testCreateValid() {
        AccountRequestDTO accountRequestDTO = new AccountRequestDTO("email@gmail.com", "test01", "password123$", 1L);
        EmployeeRequestDTO requestDTO = new EmployeeRequestDTO( "test", "+38161141415", 2.0, LocalDate.of(2000, 12, 12), 1L, accountRequestDTO);

        AccountResponseDTO accountResponseDTO = new AccountResponseDTO(1L, "email@gmail.com", "test01", new RoleDTO(1L, "USER"));
        EmployeeResponseDTO responseDTO = new EmployeeResponseDTO(1L, "test", "+38161141415", 2.0, LocalDate.of(2000, 12, 12), 1L, accountResponseDTO);

        Mockito.when(providerService.getProviderById(requestDTO.providerId())).thenReturn(provider);
        Mockito.when(employeeMapper.toEmployee(requestDTO)).thenReturn(employee);

        Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
        Mockito.when(employeeMapper.toResponseDTO(employee)).thenReturn(responseDTO);

        EmployeeResponseDTO createdEmployeeDTO = employeeService.create(requestDTO);

        assertThat(responseDTO).usingRecursiveComparison().isEqualTo(createdEmployeeDTO);
    }

    @Test
    void testValidGetById() {
        EmployeeResponseDTO employeeResponseDTO = new EmployeeResponseDTO(1L, "test", "+38161141415", 2.0, LocalDate.of(2000, 12, 12), 1L, null);
        Mockito.when(employeeMapper.toResponseDTO(employee)).thenReturn(employeeResponseDTO);

        EmployeeResponseDTO expectedEmployee = employeeMapper.toResponseDTO(employee);
        Mockito.doReturn(Optional.of(employee)).when(employeeRepository).findById(1L);
        EmployeeResponseDTO returnedEmployee = employeeService.getById(1L);
        Assertions.assertEquals(expectedEmployee, returnedEmployee);
    }

    @Test
    void testGetById_Not_Found() {
        Mockito.doReturn(Optional.empty()).when(employeeRepository).findById(1L);
        Assertions.assertThrows(NotFoundException.class, () -> employeeService.getById(1L));
    }

    @Test
    void testValidUpdate() {
        AccountUpdateDTO accountUpdateDTO = new AccountUpdateDTO("email@gmail.com",  "password123$");
        EmployeeUpdateDTO updateDTO = new EmployeeUpdateDTO("test-updated2", "+38161141415", 3.0, LocalDate.of(2000, 12, 12), accountUpdateDTO);

        AccountResponseDTO accountResponseDTO = new AccountResponseDTO(1L, "email@gmail.com", "test01", new RoleDTO(1L, "USER"));
        EmployeeResponseDTO updatedEmployeeDTO =
                new EmployeeResponseDTO(1L, updateDTO.title(), updateDTO.employeePhone(), updateDTO.ratePerHour(), updateDTO.hireDate(), 1L, accountResponseDTO);

        Mockito.when(employeeRepository.findById(1L)).thenReturn(Optional.of(employee));
        Mockito.doCallRealMethod().when(employeeMapper).updateEmployeeFromDto(updateDTO, employee);
        Mockito.when(employeeRepository.save(employee)).thenReturn(employee);
        Mockito.when(accountService.update(1L, accountUpdateDTO)).thenReturn(accountResponseDTO);
        Mockito.when(employeeMapper.toResponseDTO(employee)).thenReturn(updatedEmployeeDTO);

        var updateResponseDTO = employeeService.update(1L, updateDTO);

        assertThat(updatedEmployeeDTO).usingRecursiveComparison().isEqualTo(updateResponseDTO);
    }

    @Test
    void testUpdate_NotFound() {
        AccountUpdateDTO accountUpdateDTO = new AccountUpdateDTO("email@gmail.com",  "password123$");
        EmployeeUpdateDTO updateDTO = new EmployeeUpdateDTO("title4", "+604050255", 2.8,  LocalDate.of(2000, 12, 12), accountUpdateDTO);
        Mockito.doReturn(Optional.empty()).when(employeeRepository).findById(1L);
        Assertions.assertThrows(NotFoundException.class, () -> employeeService.update(1L, updateDTO));
    }

    @Test
    void testDelete_NotFound() {
        Mockito.doReturn(false).when(employeeRepository).existsById(1L);
        Assertions.assertThrows(NotFoundException.class, () -> employeeService.delete(1L));
    }
}
