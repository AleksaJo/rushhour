CREATE TABLE IF NOT EXISTS `providers`(
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL UNIQUE,
    website VARCHAR (255) NOT NULL,
    business_domain VARCHAR (255) NOT NULL UNIQUE,
    phone VARCHAR (255) NOT NULL,
    start_time_of_working_days TIME NOT NULL,
    end_time_of_working_days TIME NOT NULL,
    PRIMARY KEY (id),
    UNIQUE KEY UK_provider_name (name),
    UNIQUE KEY UK_business_domain (business_domain)
);