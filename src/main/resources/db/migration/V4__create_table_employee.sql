CREATE TABLE IF NOT EXISTS `employees` (
id BIGINT NOT NULL AUTO_INCREMENT,
title VARCHAR(255) NOT NULL,
employee_phone VARCHAR(255) NOT NULL,
rate_per_hour DOUBLE NOT NULL,
hire_date DATE NOT NULL,
provider_id BIGINT NOT NULL,
account_id BIGINT NOT NULL,
PRIMARY KEY (id)
);

ALTER TABLE `employees`
ADD CONSTRAINT provider_fk FOREIGN KEY (provider_id) REFERENCES `providers`(id);

ALTER TABLE `employees`
ADD CONSTRAINT account_fk FOREIGN KEY (account_id) REFERENCES `accounts`(id);