CREATE TABLE IF NOT EXISTS `provider_working_days` (
  `provider_id` bigint NOT NULL,
  `working_days` varchar(255) NULL DEFAULT "MONDAY",
  CONSTRAINT `fk_provider_id` FOREIGN KEY (`provider_id`) REFERENCES `providers` (`id`)
 );