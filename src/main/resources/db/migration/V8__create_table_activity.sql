CREATE TABLE `activities` (
id BIGINT NOT NULL AUTO_INCREMENT,
name VARCHAR(255) NOT NULL,
price DOUBLE NOT NULL,
duration BIGINT NOT NULL,
provider_id BIGINT,
PRIMARY KEY (id),
CONSTRAINT FOREIGN KEY(provider_id) REFERENCES providers(id)
);

CREATE TABLE activities_employees (
    activity_id BIGINT,
    employee_id BIGINT,
    CONSTRAINT FK_activity FOREIGN KEY (activity_id)
    REFERENCES activities(id),
    CONSTRAINT FK_employee FOREIGN KEY (employee_id)
    REFERENCES employees(id)
    ON DELETE CASCADE,
    PRIMARY KEY (activity_id, employee_id)
);