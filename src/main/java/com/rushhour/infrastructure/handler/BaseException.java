package com.rushhour.infrastructure.handler;

public class BaseException extends RuntimeException {
    public BaseException(String message) {
        super(message);
    }
}
