package com.rushhour.infrastructure.handler;

import com.rushhour.infrastructure.annotation.CoverageIgnoreGenerated;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@CoverageIgnoreGenerated
@ResponseStatus(HttpStatus.CONFLICT)
public class ConflictException extends BaseException {

    public ConflictException(String message) {
        super(message);
    }
}
