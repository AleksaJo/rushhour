package com.rushhour.infrastructure.handler;

import com.rushhour.infrastructure.annotation.CoverageIgnoreGenerated;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@CoverageIgnoreGenerated
@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends BaseException {
    public NotFoundException(String message) {
        super(message);
    }
}
