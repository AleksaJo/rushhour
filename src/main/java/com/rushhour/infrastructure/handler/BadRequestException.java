package com.rushhour.infrastructure.handler;

import com.rushhour.infrastructure.annotation.CoverageIgnoreGenerated;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@CoverageIgnoreGenerated
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends BaseException{

    public BadRequestException(String message) {
        super(message);
    }
}
