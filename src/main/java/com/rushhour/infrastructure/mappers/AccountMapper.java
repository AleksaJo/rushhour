package com.rushhour.infrastructure.mappers;

import com.rushhour.domain.account.entity.Account;
import com.rushhour.domain.account.model.AccountRequestDTO;
import com.rushhour.domain.account.model.AccountResponseDTO;
import com.rushhour.domain.account.model.AccountUpdateDTO;
import com.rushhour.domain.role.entity.Role;
import com.rushhour.domain.role.service.RoleService;
import com.rushhour.infrastructure.annotation.CoverageIgnoreGenerated;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@CoverageIgnoreGenerated
@Mapper(componentModel = "spring", uses = { RoleService.class })
public interface AccountMapper {

    @Mapping(target="role", source = "role")
    Account toAccount(AccountRequestDTO accountRequestDTO, Role role);
    AccountResponseDTO toResponseDTO(Account account);
    void updateAccount(AccountUpdateDTO update, @MappingTarget Account entity);

}
