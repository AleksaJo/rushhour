package com.rushhour.infrastructure.mappers;

import com.rushhour.domain.appointment.entity.Appointment;
import com.rushhour.domain.appointment.model.AppointmentRequestDTO;
import com.rushhour.domain.appointment.model.AppointmentResponseDTO;
import com.rushhour.domain.appointment.model.AppointmentUpdateDTO;
import com.rushhour.infrastructure.annotation.CoverageIgnoreGenerated;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@CoverageIgnoreGenerated
@Mapper(componentModel = "spring", uses = {ActivityMapper.class})
public interface AppointmentMapper {

    Appointment toAppointment(AppointmentRequestDTO requestDTO);

    @Mapping(source = "employee.provider.id", target = "employee.providerId")
    AppointmentResponseDTO toResponseDto(Appointment appointment);

    void updateAppointmentDto(AppointmentUpdateDTO updateDTO, @MappingTarget Appointment entity);

}
