package com.rushhour.infrastructure.mappers;

import com.rushhour.domain.provider.entity.Provider;
import com.rushhour.domain.provider.model.ProviderRequestDTO;
import com.rushhour.domain.provider.model.ProviderResponseDTO;
import com.rushhour.domain.provider.model.ProviderUpdateDTO;
import com.rushhour.infrastructure.annotation.CoverageIgnoreGenerated;
import org.mapstruct.*;

@CoverageIgnoreGenerated
@Mapper(componentModel = "spring")
public interface ProviderMapper {

    @Mapping(target = "id", ignore = true)
    Provider toProvider(ProviderRequestDTO providerRequestDTO);

    ProviderResponseDTO toProviderDto(Provider provider);

    @Mapping(target = "businessDomain", ignore = true)
    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateProviderFromDto(ProviderUpdateDTO providerUpdateDTO, @MappingTarget Provider entity);
}
