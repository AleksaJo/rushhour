package com.rushhour.infrastructure.mappers;

import com.rushhour.domain.employee.entity.Employee;
import com.rushhour.domain.employee.model.EmployeeRequestDTO;
import com.rushhour.domain.employee.model.EmployeeResponseDTO;
import com.rushhour.domain.employee.model.EmployeeUpdateDTO;
import com.rushhour.domain.role.service.RoleService;
import com.rushhour.infrastructure.annotation.CoverageIgnoreGenerated;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@CoverageIgnoreGenerated
@Mapper(componentModel = "spring", uses = { RoleService.class })
public interface EmployeeMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "providerId", target = "provider.id")
    @Mapping(source = "requestDTO.account.roleId", target = "account.role")
    Employee toEmployee(EmployeeRequestDTO requestDTO);

    @Mapping(source = "provider.id", target = "providerId")
    EmployeeResponseDTO toResponseDTO(Employee employee);

    @Mapping(target = "account", ignore = true)
    @Mapping(target = "provider", ignore = true)
    void updateEmployeeFromDto(EmployeeUpdateDTO employeeUpdateDTO, @MappingTarget Employee entity);
}
