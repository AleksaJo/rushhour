package com.rushhour.infrastructure.mappers;

import com.rushhour.domain.activity.entity.Activity;
import com.rushhour.domain.activity.model.ActivityRequestDTO;
import com.rushhour.domain.activity.model.ActivityResponseDTO;
import com.rushhour.domain.activity.model.ActivityUpdateDTO;
import com.rushhour.domain.employee.entity.Employee;
import com.rushhour.infrastructure.annotation.CoverageIgnoreGenerated;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Named;

import java.time.Duration;
import java.util.List;

@CoverageIgnoreGenerated
@Mapper(componentModel = "spring")
public interface ActivityMapper {

    Activity toActivity(ActivityRequestDTO activityRequestDTO);

    @Mapping(source = "activity.provider.id", target = "providerId")
    @Mapping(source = "activity.employees", target = "employeeIds", qualifiedByName = "employeesToEmployeeIds")
    @Mapping(source = "activity.duration", target = "duration", qualifiedByName = "toDurationMinutes")
    ActivityResponseDTO toResponseDTO(Activity activity);
    void updateActivity(ActivityUpdateDTO activityUpdateDTO, @MappingTarget Activity entity);

    @Named("employeesToEmployeeIds")
    default List<Long> employeesToEmployeeIds(List<Employee> employees) {
        return employees.stream()
                .map(Employee::getId)
                .toList();
    }

    @Named("toDurationMinutes")
    default String toDurationMinutes(Duration duration) {
        return String.format("%s minutes",duration.toMinutes());
    }


}
