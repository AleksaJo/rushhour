package com.rushhour.infrastructure.mappers;

import com.rushhour.domain.client.entity.Client;
import com.rushhour.domain.client.model.ClientRequestDTO;
import com.rushhour.domain.client.model.ClientResponseDTO;
import com.rushhour.domain.client.model.ClientUpdateDTO;
import com.rushhour.domain.role.service.RoleService;
import com.rushhour.infrastructure.annotation.CoverageIgnoreGenerated;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@CoverageIgnoreGenerated
@Mapper(componentModel = "spring", uses = { RoleService.class })
public interface ClientMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "account.role", ignore = true)
    Client toClient(ClientRequestDTO requestDTO);
    ClientResponseDTO toResponseDTO(Client client);

    void updateClientFromDTO(ClientUpdateDTO clientUpdateDTO, @MappingTarget Client entity);

}
