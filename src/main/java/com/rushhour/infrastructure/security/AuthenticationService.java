package com.rushhour.infrastructure.security;

import com.rushhour.domain.account.entity.Account;
import com.rushhour.domain.account.model.LoginRequestDTO;
import com.rushhour.domain.account.model.LoginResponseDTO;
import com.rushhour.domain.account.service.AccountService;
import com.rushhour.infrastructure.handler.ConflictException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthenticationService implements UserDetailsService {
    private final AccountService accountService;
    private final JwtUtil jwtUtil;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public AuthenticationService(AccountService accountService, JwtUtil jwtUtil) {
        this.accountService = accountService;
        this.jwtUtil = jwtUtil;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Account> accountOptional = accountService.findByEmail(username);
        if (accountOptional.isEmpty()) {
            throw new ConflictException("Bad credentials");
        }

        Account account = accountOptional.get();

        return new AuthenticatedUser(account);
    }

    public LoginResponseDTO authenticate(LoginRequestDTO loginRequestDTO) {
        var authenticatedUser = (AuthenticatedUser) loadUserByUsername(loginRequestDTO.username());
        if (!passwordEncoder.matches(loginRequestDTO.password(), authenticatedUser.getPassword())) {
            throw new ConflictException("Bad credentials");
        }
        var accessToken = jwtUtil.createToken(authenticatedUser);

        return new LoginResponseDTO(accessToken);
    }

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }
}
