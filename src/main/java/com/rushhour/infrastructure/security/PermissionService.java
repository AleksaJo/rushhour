package com.rushhour.infrastructure.security;

import com.rushhour.domain.activity.service.ActivityService;
import com.rushhour.domain.appointment.service.AppointmentService;
import com.rushhour.domain.client.model.ClientResponseDTO;
import com.rushhour.domain.client.service.ClientService;
import com.rushhour.domain.employee.entity.Employee;
import com.rushhour.domain.employee.service.EmployeeService;
import com.rushhour.domain.provider.service.ProviderService;
import com.rushhour.domain.role.enums.RoleNames;
import com.rushhour.domain.role.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Service("permissionService")
public class PermissionService {

    private final ClientService clientService;
    private final ProviderService providerService;
    private final EmployeeService employeeService;
    private final ActivityService activityService;
    private final RoleService roleService;
    private final AppointmentService appointmentService;

    @Autowired
    public PermissionService(ClientService clientService,
                             ProviderService providerService,
                             EmployeeService employeeService,
                             ActivityService activityService,
                             RoleService roleService, AppointmentService appointmentService) {
        this.clientService = clientService;
        this.providerService = providerService;
        this.employeeService = employeeService;
        this.activityService = activityService;
        this.roleService = roleService;
        this.appointmentService = appointmentService;
    }

    public boolean canClientAccess(Long clientId) {
        var authenticatedUser = getPrincipal();
        ClientResponseDTO client = clientService.getById(clientId);
        return client.account().id().equals(authenticatedUser.getAccount().getId());
    }

    public boolean canProviderAdminAccess(Long providerId) {
        var authenticatedUser = getPrincipal();

        return providerService.getProviderByEmployeeAccountId(authenticatedUser.getAccount().getId())
        .map(provider -> provider.getId().equals(providerId))
        .orElse(false);
    }

    public boolean canProviderAdminAccessAppointment(Long appointmentId) {
        var authenticatedUser = getPrincipal();

        var providerAdminProvider = providerService.getProviderByEmployeeAccountId(authenticatedUser.getAccount().getId());
        if (providerAdminProvider.isEmpty()) {
            return false;
        }

        var appointmentResponseDTO = appointmentService.getById(appointmentId);
        var providerId = appointmentResponseDTO.employee().providerId();

        return providerAdminProvider.get().getId().equals(providerId);
    }

    public boolean canCreateAppointmentForColleague(Long employeeId) {
        var authenticatedUser = getPrincipal();

        var loggedEmployee = employeeService.getByAccountId(authenticatedUser.getAccount().getId());

        if (loggedEmployee.isEmpty()) {
            return false;
        }

        var loggedEmployeeProviderId = loggedEmployee.get().getProvider().getId();

        var employee = employeeService.getById(employeeId);

        return loggedEmployeeProviderId.equals(employee.providerId());
    }

    public boolean canProviderAdminCreateEmployee(Long providerId, Long employeeRoleId) {
        var authenticatedUser = getPrincipal();

        var roleOptional = roleService.findById(employeeRoleId);

        if (roleOptional.isEmpty()) {
            return false;
        }

        var role = roleOptional.get();

        return providerService.getProviderByEmployeeAccountId(authenticatedUser.getAccount().getId())
                .map(provider -> provider.getId().equals(providerId)
                        && role.getName().equals(RoleNames.EMPLOYEE.name()))
                .orElse(false);
    }

    public boolean canEmployeeAccess(Long employeeId) {
        var authenticatedUser = getPrincipal();

        Optional<Employee> employeeOptional = employeeService.getByAccountId(authenticatedUser.getAccount().getId());

        if (employeeOptional.isEmpty()) {
            return false;
        }

        var employee = employeeOptional.get();
        return Objects.equals(employeeId, employee.getId());
    }

    public boolean canEmployeeAccessAppointment(Long id) {
        var authenticatedUser = getPrincipal();

        Optional<Employee> employeeOptional = employeeService.getByAccountId(authenticatedUser.getAccount().getId());

        if (employeeOptional.isEmpty()) {
            return false;
        }

        var appointment = appointmentService.getById(id);

        var appointmentProviderOptional = providerService.getProviderByEmployeeAccountId(appointment.employee().account().id());
        var loggedProviderOptional = providerService.getProviderByEmployeeAccountId(authenticatedUser.getAccount().getId());

        if (appointmentProviderOptional.isEmpty() || loggedProviderOptional.isEmpty()) {
            return false;
        }
        var appointmentProvider = appointmentProviderOptional.get();
        var loggedProvider = loggedProviderOptional.get();

        return appointmentProvider.getId().equals(loggedProvider.getId());

    }

    public boolean canClientAccessAppointment(Long id) {
        var authenticatedUser = getPrincipal();

        var optionalClient = clientService.findByAccountId(authenticatedUser.getAccount().getId());
        var appointmentOptional = appointmentService.findById(id);

        if (optionalClient.isEmpty() || appointmentOptional.isEmpty()) {
            return false;
        }

        var client = optionalClient.get();
        var appointment = appointmentOptional.get();

        return Objects.equals(client.getId(), appointment.getClient().getId());

    }

    public boolean canProviderAdminAccessEmployee(Long employeeId) {
        var authenticatedUser = getPrincipal();

        Optional<Employee> providerAdminOptional = employeeService.getByAccountId(authenticatedUser.getAccount().getId());
        Optional<Employee> employeeOptional = employeeService.findById(employeeId);

        if (providerAdminOptional.isEmpty() || employeeOptional.isEmpty()) {
            return false;
        }

        var providerAdmin = providerAdminOptional.get();
        var employee = employeeOptional.get();

        return Objects.equals(
                employee.getProvider().getId(),
                providerAdmin.getProvider().getId());
    }

    public boolean canProviderAdminAccessActivity(Long id) {
        var authenticatedUser = getPrincipal();

        Optional<Employee> providerAdminOptional = employeeService
                .getByAccountId(authenticatedUser.getAccount().getId());

        if (providerAdminOptional.isEmpty()) {
            return false;
        }

        var activity = activityService.getById(id);
        var employee = providerAdminOptional.get();

        return employee.getProvider().getId().equals(activity.providerId());
    }

    private static AuthenticatedUser getPrincipal() {
        var securityContext = SecurityContextHolder.getContext();
        return (AuthenticatedUser) securityContext
                .getAuthentication()
                .getPrincipal();
    }
}
