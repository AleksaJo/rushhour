package com.rushhour;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition(info = @Info(title = "RushHour API", version = "2.0"))
public class RushHourApplication {
	public static void main(String[] args) {
		SpringApplication.run(RushHourApplication.class, args);
	}
}
