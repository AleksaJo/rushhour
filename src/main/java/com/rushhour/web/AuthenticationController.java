package com.rushhour.web;

import com.rushhour.domain.account.model.LoginRequestDTO;
import com.rushhour.domain.account.model.LoginResponseDTO;
import com.rushhour.infrastructure.security.AuthenticationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/authenticate")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponseDTO> login(@RequestBody LoginRequestDTO loginRequestDTO) {
        var responseDTO = authenticationService.authenticate(loginRequestDTO);
        return ResponseEntity.ok(responseDTO);
    }

}
