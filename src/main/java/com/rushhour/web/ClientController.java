package com.rushhour.web;

import com.rushhour.domain.client.model.ClientRequestDTO;
import com.rushhour.domain.client.model.ClientResponseDTO;
import com.rushhour.domain.client.model.ClientUpdateDTO;
import com.rushhour.domain.client.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/clients")
public class ClientController {
    private final ClientService clientService;
    @Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @PostMapping
    public ResponseEntity<ClientResponseDTO> create(@Valid @RequestBody ClientRequestDTO clientDTO) {
        return ResponseEntity.ok(clientService.create(clientDTO));
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public ResponseEntity<Page<ClientResponseDTO>> getAll(Pageable pageable) {
        return ResponseEntity.ok(clientService.getAll(pageable));
    }

    @GetMapping("/{id}")
    @PreAuthorize("""
            hasRole('ADMINISTRATOR') ||
            (hasRole('CLIENT') && @permissionService.canClientAccess(#id))
        """)
    public ResponseEntity<ClientResponseDTO> getById(@PathVariable Long id) {
        return ResponseEntity.ok(clientService.getById(id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("""
            hasRole('ADMINISTRATOR') ||
            (hasRole('CLIENT') && @permissionService.canClientAccess(#id))
        """)
    public ResponseEntity<ClientResponseDTO> update(@PathVariable Long id, @Valid @RequestBody ClientUpdateDTO clientUpdateDTO) {
        return ResponseEntity.ok(clientService.update(id, clientUpdateDTO));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("""
            hasRole('ADMINISTRATOR') ||
            (hasRole('CLIENT') && @permissionService.canClientAccess(#id))
        """)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        clientService.delete(id);
    }
}
