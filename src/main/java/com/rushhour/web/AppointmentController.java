package com.rushhour.web;

import com.rushhour.domain.appointment.model.*;
import com.rushhour.domain.appointment.repository.AppointmentSpecification;
import com.rushhour.domain.appointment.service.AppointmentService;
import com.rushhour.infrastructure.security.AuthenticatedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/appointments")
public class AppointmentController {

    private final AppointmentService appointmentService;

    @Autowired
    public AppointmentController(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @PostMapping
    @PreAuthorize("""
            hasRole('ADMINISTRATOR') ||
            (hasRole('PROVIDER_ADMINISTRATOR') && @permissionService.canProviderAdminAccessEmployee(#requestDTO.employeeId)) ||
            (hasRole('EMPLOYEE') && @permissionService.canEmployeeAccess(#requestDTO.employeeId)) ||
            (hasRole('CLIENT') &&  @permissionService.canClientAccess(#requestDTO.clientId))
            """)
    public ResponseEntity<AppointmentResponseDTO> create(@Valid @RequestBody AppointmentRequestDTO requestDTO) {
        return ResponseEntity.ok(appointmentService.create(requestDTO));
    }

    @PutMapping("/{id}")
    @PreAuthorize("""
            hasRole('ADMINISTRATOR') ||
            (hasRole('PROVIDER_ADMINISTRATOR') && @permissionService.canProviderAdminAccessAppointment(#id)) ||
            (hasRole('EMPLOYEE') && @permissionService.canEmployeeAccessAppointment(#id)) ||
            (hasRole('CLIENT') && @permissionService.canClientAccessAppointment(#id))
            """)
    public ResponseEntity<AppointmentResponseDTO> update(@PathVariable Long id, @Valid @RequestBody AppointmentUpdateDTO updateDTO) {
        return ResponseEntity.ok(appointmentService.update(id, updateDTO));
    }

    @GetMapping("/{id}")
    @PreAuthorize("""
            hasRole('ADMINISTRATOR') ||
            (hasRole('PROVIDER_ADMINISTRATOR') && @permissionService.canProviderAdminAccessAppointment(#id)) ||
            (hasRole('EMPLOYEE') && @permissionService.canEmployeeAccessAppointment(#id)) ||
            (hasRole('CLIENT') && @permissionService.canClientAccessAppointment(#id))
            """)
    public ResponseEntity<AppointmentResponseDTO> getById(@PathVariable Long id) {
        return ResponseEntity.ok(appointmentService.getById(id));
    }

    @GetMapping
    @PreAuthorize("""
            hasRole('ADMINISTRATOR') ||
            (hasRole('PROVIDER_ADMINISTRATOR') && #specification.providerId != null && @permissionService.canProviderAdminAccess(#specification.providerId)) ||
            (hasRole('EMPLOYEE') && #specification.employeeId != null && @permissionService.canEmployeeAccess(#specification.employeeId)) ||
            (hasRole('CLIENT') && #specification.clientId != null && @permissionService.canClientAccess(#specification.clientId)) 
            """)
    public ResponseEntity<Page<AppointmentResponseDTO>> getAll(Pageable pageable, AppointmentSpecification specification) {
        return ResponseEntity.ok(appointmentService.getAll(pageable, specification));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("""
            hasRole('ADMINISTRATOR') ||
            (hasRole('PROVIDER_ADMINISTRATOR') && @permissionService.canProviderAdminAccessAppointment(#id)) ||
            (hasRole('EMPLOYEE') && @permissionService.canEmployeeAccessAppointment(#id)) ||
            (hasRole('CLIENT') && @permissionService.canClientAccessAppointment(#id))
            """)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        appointmentService.delete(id);
    }

    @PostMapping("/schedule")
    @PreAuthorize("hasRole('CLIENT')")
    public ResponseEntity<AppointmentsForClientResponseDto> scheduledAppointmentForClients(
            @Valid @RequestBody AppointmentsForClientRequestDto dto,
            @AuthenticationPrincipal AuthenticatedUser authenticatedUser) {
        return ResponseEntity.ok(appointmentService.scheduledAppointmentForClients(dto, authenticatedUser.getAccount().getId()));
    }
}
