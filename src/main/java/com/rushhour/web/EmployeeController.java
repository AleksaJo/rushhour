package com.rushhour.web;

import com.rushhour.domain.employee.model.EmployeeRequestDTO;
import com.rushhour.domain.employee.model.EmployeeResponseDTO;
import com.rushhour.domain.employee.model.EmployeeUpdateDTO;
import com.rushhour.domain.employee.service.EmployeeService;
import com.rushhour.infrastructure.security.AuthenticatedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/employees")
public class EmployeeController {
    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @PostMapping
    @PreAuthorize("""
        hasRole('ADMINISTRATOR') || 
        (hasRole('PROVIDER_ADMINISTRATOR') && @permissionService.canProviderAdminCreateEmployee(#employeeDTO.providerId, #employeeDTO.account.roleId))
        """)
    public ResponseEntity<EmployeeResponseDTO> create(@Valid @RequestBody EmployeeRequestDTO employeeDTO) {
        return ResponseEntity.ok(employeeService.create(employeeDTO));
    }

    @GetMapping
    @PreAuthorize("""
        hasRole('ADMINISTRATOR')
        """)
    public ResponseEntity<Page<EmployeeResponseDTO>> getAll(Pageable pageable) {
        return ResponseEntity.ok(employeeService.getAll(pageable));
    }

    @GetMapping("/provider")
    @PreAuthorize("""
        hasRole('PROVIDER_ADMINISTRATOR')
        """)
    public ResponseEntity<Page<EmployeeResponseDTO>> getAllForProvider(@AuthenticationPrincipal AuthenticatedUser user, Pageable pageable) {
        return ResponseEntity.ok(employeeService.getAllForProviderByAccount(pageable, user.getAccount().getId()));
    }

    @GetMapping("{id}")
    @PreAuthorize("""
        hasRole('ADMINISTRATOR') || 
        (hasRole('PROVIDER_ADMINISTRATOR') && @permissionService.canProviderAdminAccessEmployee(#id)) ||
        (hasRole('EMPLOYEE') && @permissionService.canEmployeeAccess(#id))
        """)
    public ResponseEntity<EmployeeResponseDTO> getById(@PathVariable Long id) {
        return ResponseEntity.ok(employeeService.getById(id));
    }

    @PutMapping("{id}")
    @PreAuthorize("""
        hasRole('ADMINISTRATOR') || 
        (hasRole('PROVIDER_ADMINISTRATOR') && @permissionService.canProviderAdminAccessEmployee(#id)) ||
        (hasRole('EMPLOYEE') && @permissionService.canEmployeeAccess(#id))
        """)
    public ResponseEntity<EmployeeResponseDTO> update(@PathVariable Long id, @Valid @RequestBody EmployeeUpdateDTO employeeDTO) {
        return ResponseEntity.ok(employeeService.update(id, employeeDTO));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("""
        hasRole('ADMINISTRATOR') || 
        (hasRole('PROVIDER_ADMINISTRATOR') && @permissionService.canProviderAdminAccessEmployee(#id))
        """)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        employeeService.delete(id);
    }
}
