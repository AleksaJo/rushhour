package com.rushhour.web;

import com.rushhour.domain.activity.model.ActivityRequestDTO;
import com.rushhour.domain.activity.model.ActivityResponseDTO;
import com.rushhour.domain.activity.model.ActivityUpdateDTO;
import com.rushhour.domain.activity.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/activity")
public class ActivityController {

    private final ActivityService activityService;

    @Autowired
    public ActivityController(ActivityService activityService) {
        this.activityService = activityService;
    }

    @PostMapping
    @PreAuthorize("""
            hasRole('ADMINISTRATOR') ||
            (hasRole('PROVIDER_ADMINISTRATOR') && @permissionService.canProviderAdminAccess(#activityDto.providerId))
            """)
    public ResponseEntity<ActivityResponseDTO> create(@Valid @RequestBody ActivityRequestDTO activityDto) {
        return ResponseEntity.ok(activityService.create(activityDto));
    }

    @GetMapping
    @PreAuthorize("""
            hasRole('ADMINISTRATOR') ||
            hasRole('CLIENT') ||
            (hasRole('PROVIDER_ADMINISTRATOR') && @permissionService.canProviderAdminAccess(#providerId))
            """)
    public ResponseEntity<Page<ActivityResponseDTO>> getALl(@RequestParam Long providerId, Pageable pageable) {
        return ResponseEntity.ok(activityService.getAll(providerId, pageable));
    }

    @GetMapping("/{id}")
    @PreAuthorize("""
            hasRole('ADMINISTRATOR') ||
            hasRole('CLIENT') ||
            (hasRole('PROVIDER_ADMINISTRATOR') && @permissionService.canProviderAdminAccessActivity(#id))
            """)
    public ResponseEntity<ActivityResponseDTO> getById(@PathVariable Long id) {
        return ResponseEntity.ok(activityService.getById(id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("""
            hasRole('ADMINISTRATOR') ||
            (hasRole('PROVIDER_ADMINISTRATOR') && @permissionService.canProviderAdminAccessActivity(#id))
            """)
    public ResponseEntity<ActivityResponseDTO> update(@PathVariable Long id,
                                                      @Valid @RequestBody ActivityUpdateDTO updateDTO) {
        return ResponseEntity.ok(activityService.update(id, updateDTO));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("""
            hasRole('ADMINISTRATOR') ||
            (hasRole('PROVIDER_ADMINISTRATOR') && @permissionService.canProviderAdminAccessActivity(#id))
            """)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        activityService.delete(id);
    }

}