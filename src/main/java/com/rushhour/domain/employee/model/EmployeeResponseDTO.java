package com.rushhour.domain.employee.model;

import com.rushhour.domain.account.model.AccountResponseDTO;

import java.time.LocalDate;

public record EmployeeResponseDTO(
        Long id,
        String title,
        String employeePhone,
        double ratePerHour,
        LocalDate hireDate,
        Long providerId,
        AccountResponseDTO account
) {}
