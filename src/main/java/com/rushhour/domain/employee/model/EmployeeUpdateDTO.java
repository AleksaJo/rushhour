package com.rushhour.domain.employee.model;

import com.rushhour.domain.account.model.AccountUpdateDTO;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;

public record EmployeeUpdateDTO (
        @NotBlank(message = "Employee title can not be blank.")
        @Pattern(regexp = "^([a-zA-Z0-9]){2,255}$", message = "Invalid employee title!")
        String title,

        @NotBlank(message = "Employee phone can not be blank.")
        @Pattern(regexp = "^([0-9+]){9,15}$", message = "Invalid employee phone format!")
        String employeePhone,

        @Min(value = 1, message = "Rate per hour must be greater than 1")
        double ratePerHour,

        @NotNull(message = "Date can not be empty or null")
        @DateTimeFormat(pattern = "yyyy-MM-dd")
        LocalDate hireDate,

        @Valid
        AccountUpdateDTO account

        ){}
