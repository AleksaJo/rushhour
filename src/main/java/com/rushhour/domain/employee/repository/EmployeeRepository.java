package com.rushhour.domain.employee.repository;

import com.rushhour.domain.employee.entity.Employee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    @Query("""
            SELECT e FROM Employee e
            WHERE e.account.id = :accountId
            """)
    Optional<Employee> findByAccountId(@Param("accountId") Long accountId);

    Page<Employee> findAllByProviderId(Pageable pageable, Long providerId);

    @Query("""
            SELECT e FROM Employee e
            WHERE e.id IN (:employeeIds)
            AND e.provider.id = :providerId
            """)
    List<Employee> findByIdInAndProviderId(List<Long> employeeIds, Long providerId);
}
