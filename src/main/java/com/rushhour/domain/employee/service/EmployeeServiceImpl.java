package com.rushhour.domain.employee.service;

import com.rushhour.domain.account.service.AccountService;
import com.rushhour.domain.appointment.model.AppointmentRequestDTO;
import com.rushhour.domain.employee.entity.Employee;
import com.rushhour.domain.employee.model.EmployeeRequestDTO;
import com.rushhour.domain.employee.model.EmployeeResponseDTO;
import com.rushhour.domain.employee.model.EmployeeUpdateDTO;
import com.rushhour.domain.employee.repository.EmployeeRepository;
import com.rushhour.domain.provider.entity.Provider;
import com.rushhour.domain.provider.service.ProviderService;
import com.rushhour.infrastructure.handler.NotFoundException;
import com.rushhour.infrastructure.mappers.EmployeeMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;
    private final ProviderService providerService;
    private final AccountService accountService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository,
                               EmployeeMapper employeeMapper,
                               ProviderService providerService,
                               AccountService accountService,
                               PasswordEncoder passwordEncoder) {
        this.employeeRepository = employeeRepository;
        this.employeeMapper = employeeMapper;
        this.providerService = providerService;
        this.accountService = accountService;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public EmployeeResponseDTO create(EmployeeRequestDTO employeeDTO) {
        Provider provider = providerService.getProviderById(employeeDTO.providerId());
        providerService.existsByProviderId(employeeDTO);
        accountService.assertAccountNotExists(employeeDTO.account());

        Employee employee = employeeMapper.toEmployee(employeeDTO);
        employee.setProvider(provider);
        employee.getAccount()
                .setPassword(passwordEncoder.encode(employeeDTO.account().password()));

        Employee createEmployee = employeeRepository.save(employee);
        return employeeMapper.toResponseDTO(createEmployee);
    }

    @Override
    public EmployeeResponseDTO getById(Long id) {
        return employeeMapper.toResponseDTO(getDomainById(id));
    }

    private Employee getDomainById(Long id) {
        var optionalEmployee = employeeRepository.findById(id);
        if (optionalEmployee.isEmpty()) {
            throw new NotFoundException("Employee with given id %s does not exists".formatted(id));
        }
        return optionalEmployee.get();
    }

    public Optional<Employee> getByAccountId(Long id) {
        return employeeRepository.findByAccountId(id);
    }

    @Override
    public Page<EmployeeResponseDTO> getAllForProviderByAccount(Pageable pageable, Long id) {
        Optional<Employee> employeeOptional = getByAccountId(id);

        if (employeeOptional.isEmpty()) {
            throw new NotFoundException("Employee with given account id %s does not exists".formatted(id));
        }

        Employee employee = employeeOptional.get();
        return employeeRepository.findAllByProviderId(pageable,employee.getProvider().getId()).map(employeeMapper::toResponseDTO);

    }

    @Override
    public Page<EmployeeResponseDTO> getAll(Pageable pageable) {
        return employeeRepository.findAll(pageable).map(employeeMapper::toResponseDTO);
    }

    @Override
    @Transactional
    public EmployeeResponseDTO update(Long id, EmployeeUpdateDTO employeeDTO) {
        var employee = getDomainById(id);

        employeeMapper.updateEmployeeFromDto(employeeDTO, employee);
        employeeRepository.save(employee);
        accountService.update(employee.getAccount().getId(), employeeDTO.account());
        return employeeMapper.toResponseDTO(employee);
    }

    @Override
    public void delete(Long id) {
        if (!employeeRepository.existsById(id)) {
            throw new NotFoundException("Employee with id %s does not exists to be deleted!".formatted(id));
        }
        employeeRepository.deleteById(id);
    }

    @Override
    public List<Employee> getByIds(List<Long> employeeIds) {
        return employeeRepository.findAllById(employeeIds);
    }

    @Override
    public Optional<Employee> findById(Long employeeId) {
        return employeeRepository.findById(employeeId);
    }

    @Override
    public List<Employee> getByIdsAndProviderId(List<Long> employeeIds, Long providerId) {
        return employeeRepository.findByIdInAndProviderId(employeeIds, providerId);
    }

    @Override
    public Optional<Employee> findEmployeeById(AppointmentRequestDTO requestDTO) {
        return employeeRepository.findById(requestDTO.employeeId());
    }
}
