package com.rushhour.domain.employee.service;

import com.rushhour.domain.appointment.model.AppointmentRequestDTO;
import com.rushhour.domain.employee.entity.Employee;
import com.rushhour.domain.employee.model.EmployeeRequestDTO;
import com.rushhour.domain.employee.model.EmployeeResponseDTO;
import com.rushhour.domain.employee.model.EmployeeUpdateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {
    EmployeeResponseDTO create(EmployeeRequestDTO employeeDTO);
    EmployeeResponseDTO getById(Long id);
    Optional<Employee> getByAccountId(Long id);
    Page<EmployeeResponseDTO> getAllForProviderByAccount(Pageable pageable, Long id);
    Page<EmployeeResponseDTO> getAll(Pageable pageable);
    EmployeeResponseDTO update(Long id, EmployeeUpdateDTO employeeUpdateDTO);
    void delete(Long id);
    List<Employee> getByIds(List<Long> employeeIds);
    Optional<Employee> findById(Long employeeId);
    Optional<Employee> findEmployeeById(AppointmentRequestDTO requestDTO);
    List<Employee> getByIdsAndProviderId(List<Long> employeeIds, Long providerId);
}
