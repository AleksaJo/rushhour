package com.rushhour.domain.provider.repository;

import com.rushhour.domain.provider.entity.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface ProviderRepository extends JpaRepository<Provider, Long> {
    boolean existsProviderByName(String name);
    boolean existsProviderByBusinessDomain(String businessDomain);

    @Query("""
            SELECT p FROM Provider p
            INNER JOIN Employee e
            ON e.provider.id = p.id
            WHERE e.account.id = :accountId
            """)
    Optional<Provider> findByEmployeeAccountId(@Param("accountId") Long accountId);
}
