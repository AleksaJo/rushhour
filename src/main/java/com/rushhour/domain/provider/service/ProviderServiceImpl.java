package com.rushhour.domain.provider.service;

import com.rushhour.domain.employee.model.EmployeeRequestDTO;
import com.rushhour.domain.provider.entity.Provider;
import com.rushhour.domain.provider.model.ProviderRequestDTO;
import com.rushhour.domain.provider.model.ProviderResponseDTO;
import com.rushhour.domain.provider.model.ProviderUpdateDTO;
import com.rushhour.domain.provider.repository.ProviderRepository;
import com.rushhour.infrastructure.handler.ConflictException;
import com.rushhour.infrastructure.handler.NotFoundException;
import com.rushhour.infrastructure.mappers.ProviderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProviderServiceImpl implements ProviderService {
    private final ProviderRepository providerRepository;
    private final ProviderMapper providerMapper;

    @Autowired
    public ProviderServiceImpl(ProviderRepository providerRepository, ProviderMapper providerMapper) {
        this.providerRepository = providerRepository;
        this.providerMapper = providerMapper;
    }

    @Override
    public ProviderResponseDTO create(ProviderRequestDTO providerDTO) {

        if (providerRepository.existsProviderByName(providerDTO.name())) {
            throw new ConflictException("Provider with name " + providerDTO.name() + " already exists!");
        }

        if (providerRepository.existsProviderByBusinessDomain(providerDTO.businessDomain())) {
            throw new ConflictException("Provider with business domain " + providerDTO.businessDomain() + " already exists!");
        }

        Provider provider = providerMapper.toProvider(providerDTO);
        return providerMapper.toProviderDto(providerRepository.save(provider));
    }

    @Override
    public void existsByProviderId(EmployeeRequestDTO employeeDTO) {
        var existProvider = providerRepository.existsById(employeeDTO.providerId());
        if (!existProvider) {
            throw new NotFoundException("Provider with id %s does not exists! ".formatted(employeeDTO.providerId()));
        }
    }

    @Override
    public ProviderResponseDTO getById(Long id) {
        Provider provider = getProviderById(id);
        return providerMapper.toProviderDto(provider);
    }

    @Override
    public Page<ProviderResponseDTO> getAll(Pageable pageable) {
        return providerRepository.findAll(pageable).map(providerMapper::toProviderDto);
    }

    @Override
    public Provider getProviderById(Long id) {
        return providerRepository.findById(id).orElseThrow(() ->
                new NotFoundException("Provider with id %s does not exists!".formatted(id)));
    }

    @Override
    public Optional<Provider> getProviderByEmployeeAccountId(Long id) {
        return providerRepository
                .findByEmployeeAccountId(id);
    }

    @Override
    public ProviderResponseDTO update(Long id, ProviderUpdateDTO providerDTO) {
        var optionalProvider = getProviderById(id);

        providerMapper.updateProviderFromDto(providerDTO, optionalProvider);
        providerRepository.save(optionalProvider);
        return providerMapper.toProviderDto(optionalProvider);
    }

    @Override
    public void delete(Long id) {
        if (!providerRepository.existsById(id)) {
            throw new NotFoundException("Provider with id %s does not exists.".formatted(id));
        }
         providerRepository.deleteById(id);
    }
}
