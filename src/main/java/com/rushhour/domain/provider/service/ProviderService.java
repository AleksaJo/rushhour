package com.rushhour.domain.provider.service;

import com.rushhour.domain.employee.model.EmployeeRequestDTO;
import com.rushhour.domain.provider.entity.Provider;
import com.rushhour.domain.provider.model.ProviderRequestDTO;
import com.rushhour.domain.provider.model.ProviderResponseDTO;
import com.rushhour.domain.provider.model.ProviderUpdateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ProviderService {
    ProviderResponseDTO create(ProviderRequestDTO providerRequestDTO);
    ProviderResponseDTO getById(Long id);
    Page<ProviderResponseDTO> getAll(Pageable pageable);
    Provider getProviderById(Long id);
    Optional<Provider> getProviderByEmployeeAccountId(Long id);
    void existsByProviderId(EmployeeRequestDTO requestDTO);
    ProviderResponseDTO update(Long id, ProviderUpdateDTO providerUpdateDTO);
    void delete (Long id);
}
