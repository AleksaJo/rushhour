package com.rushhour.domain.provider.model;

import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Set;

public record ProviderUpdateDTO(

        @NotBlank(message = "Name can not be blank.")
        @Pattern(regexp = "^([a-zA-Z]){3,}$", message = "Invalid name!")
        String name,

        @NotBlank(message = "Website can not be blank.")
        @URL(message = "Invalid website URL!")
        String website,

        @NotBlank(message = "Phone can not be blank.")
        @Pattern(regexp = "^([0-9+]){9,15}$", message = "Invalid phone format!")
        String phone,

        @NotNull(message = "Start time of working days can not be blank.")
        LocalTime startTimeOfWorkingDays,

        @NotNull(message = "End time of working days can not be blank.")
        LocalTime endTimeOfWorkingDays,

        Set<DayOfWeek> workingDays

) {}
