package com.rushhour.domain.provider.model;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Set;

public record ProviderResponseDTO(

        Long id,

        String name,

        String website,

        String businessDomain,

        String phone,

        LocalTime startTimeOfWorkingDays,

        LocalTime endTimeOfWorkingDays,

        Set<DayOfWeek> workingDays

) {}
