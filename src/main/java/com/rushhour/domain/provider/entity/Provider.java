package com.rushhour.domain.provider.entity;

import com.rushhour.domain.activity.entity.Activity;
import com.rushhour.domain.employee.entity.Employee;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "providers")
public class Provider {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String website;

    @Column(nullable = false, unique = true)
    private String businessDomain;

    @Column(nullable = false)
    private String phone;

    @Column(nullable = false)
    private LocalTime startTimeOfWorkingDays;

    @Column(nullable = false)
    private LocalTime endTimeOfWorkingDays;

    @Enumerated(EnumType.STRING)
    @ElementCollection(targetClass = DayOfWeek.class)
    private Set<DayOfWeek> workingDays = new HashSet<>();

    @OneToMany(mappedBy = "provider", cascade = CascadeType.ALL)
    private List<Employee> employees;

    @OneToMany(mappedBy = "provider", cascade = CascadeType.ALL)
    private List<Activity> activities;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getBusinessDomain() {
        return businessDomain;
    }

    public void setBusinessDomain(String businessDomain) {
        this.businessDomain = businessDomain;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LocalTime getStartTimeOfWorkingDays() {
        return startTimeOfWorkingDays;
    }

    public void setStartTimeOfWorkingDays(LocalTime startTimeOfWorkingDays) {
        this.startTimeOfWorkingDays = startTimeOfWorkingDays;
    }

    public LocalTime getEndTimeOfWorkingDays() {
        return endTimeOfWorkingDays;
    }

    public void setEndTimeOfWorkingDays(LocalTime endTimeOfWorkingDays) {
        endTimeOfWorkingDays.truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_LOCAL_TIME);
        this.endTimeOfWorkingDays = endTimeOfWorkingDays;
    }

    public Set<DayOfWeek> getWorkingDays() {
        return workingDays;
    }

    public void setWorkingDays(Set<DayOfWeek> workingDays) {
        this.workingDays = workingDays;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
}
