package com.rushhour.domain.client.repository;

import com.rushhour.domain.client.entity.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {

    @Query("""
            SELECT c FROM Client c
            WHERE c.account.id = :clientAccountId
            """)
    Optional<Client> findByAccountId(Long clientAccountId);
}
