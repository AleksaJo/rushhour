package com.rushhour.domain.client.model;

import com.rushhour.domain.account.model.AccountResponseDTO;

public record ClientResponseDTO(
        Long id,
        String clientPhone,
        String clientAddress,
        AccountResponseDTO account
) {}
