package com.rushhour.domain.client.model;

import com.rushhour.domain.account.model.AccountUpdateDTO;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public record ClientUpdateDTO(
        @NotBlank(message = "Client phone can not be empty!")
        @Pattern(regexp = "^([0-9+]){9,15}$", message = "Invalid client phone!")
        String clientPhone,
        @NotBlank(message = "Client address can not be empty!")
        @Pattern(regexp = "^([A-Za-z0-9]){3,}$", message = "Invalid client address!")
        String clientAddress,

        @Valid
        AccountUpdateDTO account

) {}
