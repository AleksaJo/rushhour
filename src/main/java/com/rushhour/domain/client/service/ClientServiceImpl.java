package com.rushhour.domain.client.service;

import com.rushhour.domain.account.service.AccountService;
import com.rushhour.domain.client.entity.Client;
import com.rushhour.domain.client.model.ClientRequestDTO;
import com.rushhour.domain.client.model.ClientResponseDTO;
import com.rushhour.domain.client.model.ClientUpdateDTO;
import com.rushhour.domain.client.repository.ClientRepository;
import com.rushhour.domain.role.entity.Role;
import com.rushhour.domain.role.enums.RoleNames;
import com.rushhour.domain.role.service.RoleService;
import com.rushhour.infrastructure.handler.NotFoundException;
import com.rushhour.infrastructure.mappers.ClientMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {
    private final ClientRepository clientRepository;
    private final ClientMapper clientMapper;
    private final AccountService accountService;
    private final PasswordEncoder passwordEncoder;
    private final RoleService roleService;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository,
                             ClientMapper clientMapper,
                             AccountService accountService,
                             PasswordEncoder passwordEncoder,
                             RoleService roleService) {
        this.clientRepository = clientRepository;
        this.clientMapper = clientMapper;
        this.accountService = accountService;
        this.passwordEncoder = passwordEncoder;
        this.roleService = roleService;
    }

    @Override
    public ClientResponseDTO create(ClientRequestDTO clientDTO) {
        accountService.assertAccountNotExists(clientDTO.account());
        Role role = roleService.getRoleByName(RoleNames.CLIENT.name());
        Client client = clientMapper.toClient(clientDTO);
        client.getAccount().setPassword(passwordEncoder.encode(clientDTO.account().password()));
        client.getAccount().setRole(role);
        Client createClient = clientRepository.save(client);
        return clientMapper.toResponseDTO(createClient);
    }

    public Client getClientById(Long id) {
        var optionalClient = clientRepository.findById(id);
        if (optionalClient.isEmpty()) {
            throw new NotFoundException("Client with id %s does not exists!".formatted(id));
        }
        return optionalClient.get();
    }

    @Override
    public Optional<Client> findById(Long id) {
        return clientRepository.findById(id);
    }

    @Override
    public ClientResponseDTO getById(Long id) {
        return clientMapper.toResponseDTO(getClientById(id));
    }

    @Override
    public Page<ClientResponseDTO> getAll(Pageable pageable) {
        return clientRepository.findAll(pageable).map(clientMapper::toResponseDTO);
    }

    @Override
    @Transactional
    public ClientResponseDTO update(Long id, ClientUpdateDTO clientDTO) {
        var client = getClientById(id);
        clientMapper.updateClientFromDTO(clientDTO, client);
        clientRepository.save(client);
        accountService.update(client.getAccount().getId(), clientDTO.account());
        return clientMapper.toResponseDTO(client);
    }

    @Override
    public void delete(Long id) {
        if(!clientRepository.existsById(id)) {
            throw new NotFoundException("Client with id %s does not exists!".formatted(id));
        }
        clientRepository.deleteById(id);
    }

    @Override
    public Client getClientByAccountId(Long clientAccountId) {
        return clientRepository.findByAccountId(clientAccountId)
                .orElseThrow(() -> new NotFoundException("Client with account id %s does not exists!".formatted(clientAccountId)));
    }

    @Override
    public Optional<Client> findByAccountId(Long accountId) {
        return clientRepository.findByAccountId(accountId);
    }
}
