package com.rushhour.domain.client.service;

import com.rushhour.domain.client.entity.Client;
import com.rushhour.domain.client.model.ClientRequestDTO;
import com.rushhour.domain.client.model.ClientResponseDTO;
import com.rushhour.domain.client.model.ClientUpdateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface ClientService {
ClientResponseDTO create(ClientRequestDTO requestDTO);
ClientResponseDTO getById(Long id);
Page<ClientResponseDTO> getAll(Pageable pageable);
ClientResponseDTO update(Long id, ClientUpdateDTO clientUpdateDTO);
Client getClientById(Long id);
Optional<Client> findById(Long id);
Optional<Client> findByAccountId(Long accountId);
void delete(Long id);
Client getClientByAccountId(Long clientAccountId);

}
