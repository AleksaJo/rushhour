package com.rushhour.domain.role.model;

public record RoleDTO(
        Long id,
        String name
) {}