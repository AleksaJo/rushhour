package com.rushhour.domain.role.enums;

public enum RoleNames {
    ADMINISTRATOR,
    PROVIDER_ADMINISTRATOR,
    EMPLOYEE,
    CLIENT
}
