package com.rushhour.domain.account.service;

import com.rushhour.domain.account.entity.Account;
import com.rushhour.domain.account.model.AccountRequestDTO;
import com.rushhour.domain.account.model.AccountResponseDTO;
import com.rushhour.domain.account.model.AccountUpdateDTO;
import com.rushhour.domain.account.repository.AccountRepository;
import com.rushhour.domain.role.entity.Role;
import com.rushhour.domain.role.repository.RoleRepository;
import com.rushhour.infrastructure.handler.ConflictException;
import com.rushhour.infrastructure.handler.NotFoundException;
import com.rushhour.infrastructure.mappers.AccountMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;
    private final RoleRepository roleRepository;
    private final AccountMapper accountMapper;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository, RoleRepository roleRepository, AccountMapper accountMapper) {
        this.accountRepository = accountRepository;
        this.roleRepository = roleRepository;
        this.accountMapper = accountMapper;
    }

    @Override
    public Account create(AccountRequestDTO accountDTO) {
        if (accountRepository.existsAccountByEmail(accountDTO.email())) {
            throw new ConflictException("Account with email %s already exists.".formatted(accountDTO.email()));
        }
        Role role = roleRepository.findById(accountDTO.roleId()).orElseThrow(() ->
                new NotFoundException("Role with given id %s does not exists in data base".formatted(accountDTO.roleId())));

        Account account = accountMapper.toAccount(accountDTO, role);
        return accountRepository.save(account);
    }

    @Override
    public AccountResponseDTO getById(Long id) {
        Account account = accountRepository.findById(id).orElseThrow(() ->
                new NotFoundException("Account with given id %s does not exists in data base".formatted(id)));
        return accountMapper.toResponseDTO(account);
    }

    @Override
    public Page<AccountResponseDTO> getAll(Pageable pageable) {
        return accountRepository.findAll(pageable).map(accountMapper::toResponseDTO);
    }

    @Override
    public AccountResponseDTO update(Long id, AccountUpdateDTO accountDTO) {
        var account = accountRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Account with given id %s, does not exists".formatted(id)));

        accountMapper.updateAccount(accountDTO, account);
        accountRepository.save(account);
        return accountMapper.toResponseDTO(account);
    }

    @Override
    public void assertAccountNotExists(AccountRequestDTO accountRequestDTO) {
        var accountEmail = accountRequestDTO.email();

        var alreadyExists = accountRepository.existsAccountByEmail(accountEmail);

        if (alreadyExists) {
            throw new ConflictException("User with email %s, is already present.".formatted(accountEmail));
        }
    }

    @Override
    public void delete(Long id) {
        if (!accountRepository.existsById(id)) {
            throw new NotFoundException("Account with given id %s does not exists in data base to be deleted.".formatted(id));
        }
        accountRepository.deleteById(id);
    }

    @Override
    public Optional<Account> findByEmail(String email) {
        return accountRepository.findByEmail(email);
    }
}