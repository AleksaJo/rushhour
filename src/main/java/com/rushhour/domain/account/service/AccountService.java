package com.rushhour.domain.account.service;

import com.rushhour.domain.account.entity.Account;
import com.rushhour.domain.account.model.AccountRequestDTO;
import com.rushhour.domain.account.model.AccountResponseDTO;
import com.rushhour.domain.account.model.AccountUpdateDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface AccountService {

    Account create(AccountRequestDTO accountDTO);

    AccountResponseDTO getById(Long id);

    Page<AccountResponseDTO> getAll(Pageable pageable);

    AccountResponseDTO update(Long id, AccountUpdateDTO accountDTO);

    void assertAccountNotExists(AccountRequestDTO accountRequestDTO);

    void delete(Long id);

    Optional<Account> findByEmail(String email);
}
