package com.rushhour.domain.account.model;

import com.rushhour.domain.account.validation.ValidPassword;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public record AccountRequestDTO(
        @NotBlank(message = "Email can not be empty")
        @Email(message = "Email is not in proper format")
        String email,

        @Pattern(regexp = "^([A-Za-z'-]){3,30}$", message = "Invalid full name!")
        String fullName,

        @ValidPassword
        String password,

        Long roleId
) {}
