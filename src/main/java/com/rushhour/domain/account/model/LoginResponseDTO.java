package com.rushhour.domain.account.model;

public record LoginResponseDTO(
        String accessToken
) {}
