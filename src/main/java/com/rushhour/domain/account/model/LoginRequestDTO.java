package com.rushhour.domain.account.model;

public record LoginRequestDTO(
        String username,
        String password
) {}
