package com.rushhour.domain.account.model;

import com.rushhour.domain.account.validation.ValidPassword;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

public record AccountUpdateDTO(
        @Pattern(regexp = "^([A-Za-z'-]){3,30}$", message = "Invalid full name!")
        String fullName,
        @NotNull(message= "Password must be provided")
        @ValidPassword(message = "Provided password is invalid")
        String password
) {}
