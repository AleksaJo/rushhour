package com.rushhour.domain.account.model;

import com.rushhour.domain.role.model.RoleDTO;

public record AccountResponseDTO(
        Long id,
        String email,
        String fullName,
        RoleDTO role
) {}
