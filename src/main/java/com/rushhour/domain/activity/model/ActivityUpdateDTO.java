package com.rushhour.domain.activity.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.*;
import java.time.Duration;
import java.util.List;

public record ActivityUpdateDTO(
        @NotBlank(message = "Activity name can not be blank.")
        @Pattern(regexp = "^([a-zA-Z]){2,}$", message = "Invalid activity name!")
        String name,

        @Min(value = 1, message = "Activity price must be greater than 1")
        double price,

        @NotNull(message = "Duration for activity can not be null!")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MINUTES")
        @Schema(example = "15")
        Duration duration,

        @NotEmpty(message = "At least one employee must be added to the activity!")
        List<Long> employeeIds
) {}
