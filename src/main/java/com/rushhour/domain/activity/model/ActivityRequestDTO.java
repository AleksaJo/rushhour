package com.rushhour.domain.activity.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.*;
import java.time.Duration;
import java.util.List;

public record ActivityRequestDTO(
        @NotBlank(message = "Activity name can not be blank.")
        @Size(min = 2, message = "Activity name must contain at least 2 characters!")
        @Pattern(regexp = "(([a-zA-Z]+\s?[a-zA-Z]*)+)", message = "Invalid activity name!")
        String name,

        @Min(value = 1, message = "Activity price must be greater than 1")
        double price,

        @NotNull(message = "Duration for activity can not be null!")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MINUTES")
        @Schema(example = "15")
        Duration duration,

        @NotNull(message = "Provider id can not be null!")
        Long providerId,

        @NotEmpty(message = "At least one employee must be added to the activity!")
        List<Long> employeeIds
) {}
