package com.rushhour.domain.activity.model;

import java.util.List;
public record ActivityResponseDTO(
        Long id,
        String name,
        double price,
        String duration,
        Long providerId,
        List<Long> employeeIds

) {}
