package com.rushhour.domain.activity.service;

import com.rushhour.domain.activity.entity.Activity;
import com.rushhour.domain.activity.model.ActivityRequestDTO;
import com.rushhour.domain.activity.model.ActivityResponseDTO;
import com.rushhour.domain.activity.model.ActivityUpdateDTO;
import com.rushhour.domain.activity.repository.ActivityRepository;
import com.rushhour.domain.appointment.entity.Appointment;
import com.rushhour.domain.appointment.model.AppointmentRequestDTO;
import com.rushhour.domain.appointment.service.AppointmentService;
import com.rushhour.domain.client.service.ClientService;
import com.rushhour.domain.employee.entity.Employee;
import com.rushhour.domain.employee.service.EmployeeService;
import com.rushhour.domain.provider.service.ProviderService;
import com.rushhour.infrastructure.email.EmailSender;
import com.rushhour.infrastructure.handler.BadRequestException;
import com.rushhour.infrastructure.handler.ConflictException;
import com.rushhour.infrastructure.handler.NotFoundException;
import com.rushhour.infrastructure.mappers.ActivityMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

@Service
public class ActivityServiceImpl implements ActivityService {
    private final ActivityRepository activityRepository;

    private final ProviderService providerService;

    private final EmployeeService employeeService;

    private final ActivityMapper activityMapper;

    private final ClientService clientService;

    private final EmailSender sender;

    private AppointmentService appointmentService;

    @Autowired
    public ActivityServiceImpl(ActivityRepository activityRepository,
                               ProviderService providerService,
                               EmployeeService employeeService,
                               ActivityMapper activityMapper,
                               ClientService clientService,
                               EmailSender sender) {

        this.activityRepository = activityRepository;
        this.providerService = providerService;
        this.employeeService = employeeService;
        this.activityMapper = activityMapper;
        this.clientService = clientService;
        this.sender = sender;
    }

    public AppointmentService getAppointmentService() {
        return appointmentService;
    }

    @Lazy
    @Autowired
    public void setAppointmentService(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @Override
    public ActivityResponseDTO create(ActivityRequestDTO activityDto) {
        var activity = activityMapper.toActivity(activityDto);
        var provider = providerService.getProviderById(activityDto.providerId());
        var employees = employeeService.getByIdsAndProviderId(activityDto.employeeIds(), activityDto.providerId());

        if (employees.size() != activityDto.employeeIds().size()) {
            throw new BadRequestException("Some of the employees is not from this provider!");
        }

        activity.setProvider(provider);
        activity.setEmployees(employees);

        var providerId = activityDto.providerId();
        var activityName = activityRepository.existsByNameAndProviderId(activityDto.name(), providerId);
        if (activityName) {
            throw new ConflictException("Activity with the same name %s can not be added".formatted(activity.getName()));
        }

        activityRepository.save(activity);

        return activityMapper.toResponseDTO(activity);
    }

    @Override
    public ActivityResponseDTO getById(Long id) {
        var activity = getActivityById(id);
        return activityMapper.toResponseDTO(activity);
    }

    @Override
    public ActivityResponseDTO update(Long id, ActivityUpdateDTO updateDTO) {

        var activity = getActivityById(id);
        var provider = activity.getProvider();

        if (activityRepository.existsByNameAndProviderIdAndDurationAndPrice(updateDTO.name(), provider.getId(), updateDTO.duration(), updateDTO.price())) {
            SortedSet<Long> providedEmployees = new TreeSet<>(updateDTO.employeeIds());
            SortedSet<Long> activityEmployees = new TreeSet<>(activity.getEmployees().stream().map(Employee::getId).collect(Collectors.toSet()));

            if (providedEmployees.equals(activityEmployees)) {
                throw new ConflictException("Provider with id %s can not have same activity".formatted(provider.getId()));
            }
        }

        activityMapper.updateActivity(updateDTO, activity);
        var employees = employeeService.getByIdsAndProviderId(updateDTO.employeeIds(), activity.getProvider().getId());

        if (employees.size() != updateDTO.employeeIds().size()) {
            throw new BadRequestException("Some of the employees is not from this provider!");
        }

        activity.setEmployees(employees);

        activityRepository.save(activity);

        return activityMapper.toResponseDTO(activity);
    }

    @Override
    public Page<ActivityResponseDTO> getAll(Long providerId, Pageable pageable) {
        var activities = activityRepository.findAllByProviderId(providerId, pageable);
        return activities.map(activityMapper::toResponseDTO);
    }

    @Override
    public void delete(Long id) {
    var appointments = appointmentService.getAppointmentsByActivityId(id);

    for(Appointment appointment : appointments) {
        var client = clientService.getClientById(appointment.getClient().getId());
        sender.sendTextMessage(client.getAccount().getEmail(), "Appointment deleted", "The activity no longer exists, so your appointment cannot be made.");
    }

        var activity = getActivityById(id);
        activityRepository.delete(activity);
    }

    @Override
    public List<Activity> getAllByProviderIdAndActivityIds(Long providerId, List<Long> activityIds) {
        return activityRepository.getAllByProviderIdAndActivityIds(providerId, activityIds);
    }

    public Activity getActivityById(Long id) {
        var optionalActivity = activityRepository.findById(id);
        if (optionalActivity.isEmpty()) {
            throw new NotFoundException("Activity with given id %s does not exists".formatted(id));
        }
        return optionalActivity.get();
    }

    @Override
    public Optional<Activity> findActivityById(AppointmentRequestDTO appointmentDto) {
        return activityRepository.findById(appointmentDto.activityId());
    }
}
