package com.rushhour.domain.activity.service;

import com.rushhour.domain.activity.entity.Activity;
import com.rushhour.domain.activity.model.ActivityRequestDTO;
import com.rushhour.domain.activity.model.ActivityResponseDTO;
import com.rushhour.domain.activity.model.ActivityUpdateDTO;
import com.rushhour.domain.appointment.model.AppointmentRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface ActivityService {
    ActivityResponseDTO create(ActivityRequestDTO activityDto);
    ActivityResponseDTO getById(Long id);
    ActivityResponseDTO update(Long id, ActivityUpdateDTO updateDTO);
    Page<ActivityResponseDTO> getAll(Long providerId, Pageable pageable);
    Activity getActivityById(Long id);
    Optional<Activity> findActivityById(AppointmentRequestDTO appointmentDto);
    void delete(Long id);

    List<Activity> getAllByProviderIdAndActivityIds(Long providerId, List<Long> activityIds);
}
