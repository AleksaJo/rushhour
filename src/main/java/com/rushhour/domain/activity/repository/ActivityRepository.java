package com.rushhour.domain.activity.repository;

import com.rushhour.domain.activity.entity.Activity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.Duration;
import java.util.List;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Long> {

    @Query("""
            SELECT a FROM Activity a
            WHERE a.provider.id = :providerId
            """)
    Page<Activity> findAllByProviderId(Long providerId, Pageable pageable);
    boolean existsByNameAndProviderId(String name, Long providerId);

    @Query("""
            SELECT COUNT(a) > 0 FROM Activity a
            WHERE a.name = :name
            AND a.provider.id = :providerId
            AND a.duration = :duration
            AND a.price = :price
            """)
    boolean existsByNameAndProviderIdAndDurationAndPrice(String name, Long providerId, Duration duration, double price);


    @Query("""
            SELECT a FROM Activity a
            WHERE a.provider.id = :providerId
            AND
            a.id IN (:activityIds)
            """)
    List<Activity> getAllByProviderIdAndActivityIds(Long providerId, List<Long> activityIds);
}
