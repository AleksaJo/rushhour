package com.rushhour.domain.activity.repository.converter;

import javax.persistence.AttributeConverter;
import java.time.Duration;

public class DurationInMinutesConverter implements AttributeConverter<Duration, Long> {

    @Override
    public Long convertToDatabaseColumn(Duration duration) {
        return duration.toMinutes();
    }

    @Override
    public Duration convertToEntityAttribute(Long aLong) {
        return Duration.ofMinutes(aLong);
    }
}
