package com.rushhour.domain.appointment.repository;

import com.rushhour.domain.appointment.entity.Appointment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
    Page<Appointment> findAll(Specification<Appointment> specification, Pageable pageable);

    @Query("""
           SELECT a FROM Appointment a
           WHERE a.employee.id = :employeeId
           AND a.startDate <= :endDate AND a.endDate >= :startDate
           """)
    List<Appointment> getAllAppointmentsThatOverlap(LocalDateTime startDate, LocalDateTime endDate, Long employeeId);

    List<Appointment> findAllByActivityId(Long activityId);
}
