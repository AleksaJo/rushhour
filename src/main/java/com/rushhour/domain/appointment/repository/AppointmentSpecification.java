package com.rushhour.domain.appointment.repository;

import com.rushhour.domain.appointment.entity.Appointment;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;

import static java.util.Objects.nonNull;

public class AppointmentSpecification implements Specification<Appointment> {

    private Long providerId;
    private Long employeeId;
    private Long clientId;

    @Override
    public Predicate toPredicate(Root<Appointment> root,
                                 CriteriaQuery<?> query,
                                 CriteriaBuilder criteriaBuilder) {

        var predicate = criteriaBuilder.conjunction();
        var expressions = predicate.getExpressions();

        Join<Object, Object> employee = root.join("employee");
        Join<Object, Object> provider = employee.join("provider");
        Join<Object, Object> client = root.join("client");

        if (nonNull(providerId)) {
            expressions.add(criteriaBuilder.equal(provider.get("id"), providerId));
        }

        if (nonNull(employeeId)) {
            expressions.add(criteriaBuilder.equal(employee.get("id"), employeeId));
        }

        if (nonNull(clientId)) {
            expressions.add(criteriaBuilder.equal(client.get("id"), clientId));
        }

        return predicate;
    }

    public Long getProviderId() {
        return providerId;
    }

    public void setProviderId(Long providerId) {
        this.providerId = providerId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }
}
