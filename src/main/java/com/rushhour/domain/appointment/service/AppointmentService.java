package com.rushhour.domain.appointment.service;

import com.rushhour.domain.appointment.entity.Appointment;
import com.rushhour.domain.appointment.model.*;
import com.rushhour.domain.appointment.repository.AppointmentSpecification;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface AppointmentService {
    LocalDateTime calculateEndDate(Duration duration, LocalDateTime startDate);
    AppointmentResponseDTO create(AppointmentRequestDTO appointmentRequestDTO);
    AppointmentResponseDTO getById(Long id);
    Optional<Appointment> findById(Long id);
    Page<AppointmentResponseDTO> getAll(Pageable pageable, AppointmentSpecification specification);
    AppointmentResponseDTO update(Long id, AppointmentUpdateDTO updateDTO);
    List<Appointment> getAppointmentsByActivityId(Long activityId);
    void delete(Long id);
    AppointmentsForClientResponseDto scheduledAppointmentForClients(AppointmentsForClientRequestDto dto, Long clientAccountId);
}
