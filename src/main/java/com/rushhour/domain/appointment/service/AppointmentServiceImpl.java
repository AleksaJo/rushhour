package com.rushhour.domain.appointment.service;

import com.rushhour.domain.activity.service.ActivityService;
import com.rushhour.domain.appointment.entity.Appointment;
import com.rushhour.domain.appointment.model.*;
import com.rushhour.domain.appointment.repository.AppointmentRepository;
import com.rushhour.domain.appointment.repository.AppointmentSpecification;
import com.rushhour.domain.client.service.ClientService;
import com.rushhour.domain.employee.entity.Employee;
import com.rushhour.domain.employee.service.EmployeeService;
import com.rushhour.infrastructure.email.EmailSender;
import com.rushhour.infrastructure.handler.BadRequestException;
import com.rushhour.infrastructure.handler.ConflictException;
import com.rushhour.infrastructure.handler.NotFoundException;
import com.rushhour.infrastructure.mappers.AppointmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public class AppointmentServiceImpl implements AppointmentService{
    private static final String APPOINTMENT_NOT_EXISTS = "Appointment with id %s does not exists!";
    private static final String OUTSIDE_OF_WORKING_HOURS = "This is outside of working hours!";
    private static final String TIMEFRAME_ALREADY_TAKEN = "Timeframe is already taken!";

    private final AppointmentRepository appointmentRepository;
    private ActivityService activityService;
    private final ClientService clientService;
    private final EmployeeService employeeService;
    private final AppointmentMapper appointmentMapper;
    private final EmailSender emailSender;

    @Autowired
    public AppointmentServiceImpl(AppointmentRepository appointmentRepository,
                                  ClientService clientService,
                                  EmployeeService employeeService,
                                  AppointmentMapper appointmentMapper,
                                  EmailSender emailSender) {
        this.appointmentRepository = appointmentRepository;
        this.clientService = clientService;
        this.employeeService = employeeService;
        this.appointmentMapper = appointmentMapper;
        this.emailSender = emailSender;
    }

    @Override
    public LocalDateTime calculateEndDate(Duration duration, LocalDateTime startDate) {
        return startDate.plusNanos(duration.toNanos());
    }

    @Override
    public AppointmentResponseDTO create(AppointmentRequestDTO appointmentDTO) {

        var appointment = appointmentMapper.toAppointment(appointmentDTO);

        var activity = activityService.getActivityById(appointmentDTO.activityId());
        var employeesForActivity = activity.getEmployees();
        Optional<Employee> employeeOptional = employeeService.findById(appointmentDTO.employeeId());
        var client = clientService.getClientById(appointmentDTO.clientId());

        appointment.setEndDate(calculateEndDate(activity.getDuration(),appointmentDTO.startDate()));

        var outsideOfWorkingEndHours =
        (appointment.getEndDate().getHour() > activityService.getActivityById(appointmentDTO.activityId()).getProvider().getEndTimeOfWorkingDays().getHour()) ||

        (appointment.getEndDate().getHour() == activityService.getActivityById(appointmentDTO.activityId()).getProvider().getEndTimeOfWorkingDays().getHour() &&
        appointment.getEndDate().getMinute() > activityService.getActivityById(appointmentDTO.activityId()).getProvider().getEndTimeOfWorkingDays().getMinute());

        if (outsideOfWorkingEndHours)  {
            throw new ConflictException(OUTSIDE_OF_WORKING_HOURS);
        }

        var outsideOfWorkingStartHours =
        (appointment.getStartDate().getHour() < activityService.getActivityById(appointmentDTO.activityId()).getProvider().getStartTimeOfWorkingDays().getHour()) ||

        (appointment.getStartDate().getHour() == activityService.getActivityById(appointmentDTO.activityId()).getProvider().getStartTimeOfWorkingDays().getHour() &&
        appointment.getStartDate().getMinute() < activityService.getActivityById(appointmentDTO.activityId()).getProvider().getStartTimeOfWorkingDays().getMinute());

        if (outsideOfWorkingStartHours) {
            throw new ConflictException(OUTSIDE_OF_WORKING_HOURS);
        }

        if (appointmentRepository.getAllAppointmentsThatOverlap(appointment.getStartDate(),appointment.getEndDate(),appointmentDTO.employeeId()).size() > 0) {
           throw new ConflictException(TIMEFRAME_ALREADY_TAKEN);
        }

        if (employeeOptional.isEmpty()) throw new NotFoundException("Employee not found");

        var employee = employeeOptional.get();

        boolean isEmployeeOnActivity = employeesForActivity.stream()
                .map(Employee::getId)
                .anyMatch(id -> id.equals(employee.getId()));

        if (!isEmployeeOnActivity) throw new ConflictException("Activity with id %s is not accessable by employee %s".formatted(appointmentDTO.activityId(), appointmentDTO.employeeId()));

        appointment.setEmployee(employeeOptional.get());
        appointment.setClient(client);
        appointment.setActivity(activity);

        var createdAppointment = appointmentRepository.save(appointment);

        sendAppointmentMail(
                createdAppointment.getEmployee().getAccount().getEmail(),
                createdAppointment.getStartDate()
        );

        sendAppointmentMail(
                createdAppointment.getClient().getAccount().getEmail(),
                createdAppointment.getStartDate()
        );

        return appointmentMapper.toResponseDto(createdAppointment);

    }

    @Override
    public AppointmentResponseDTO update(Long id, AppointmentUpdateDTO updateDTO) {
        var optionalAppointment = appointmentRepository.findById(id);
        if (optionalAppointment.isEmpty()) {
            throw new NotFoundException(APPOINTMENT_NOT_EXISTS.formatted(id));
        }
        var appointment = optionalAppointment.get();
        appointment.setStartDate(updateDTO.startDate());

        var activity = activityService.getActivityById(updateDTO.activityId());

        appointment.setEndDate(calculateEndDate(activity.getDuration(),updateDTO.startDate()));

        var outsideOfWorkingEndHours = (appointment.getEndDate().getHour() > activityService.getActivityById(updateDTO.activityId()).getProvider().getEndTimeOfWorkingDays().getHour()) ||
        (appointment.getEndDate().getHour() == activityService.getActivityById(updateDTO.activityId()).getProvider().getEndTimeOfWorkingDays().getHour() &&
        appointment.getEndDate().getMinute() > activityService.getActivityById(updateDTO.activityId()).getProvider().getEndTimeOfWorkingDays().getMinute());

        if (outsideOfWorkingEndHours)  {
            throw new ConflictException(OUTSIDE_OF_WORKING_HOURS);
        }

        if (appointmentRepository.getAllAppointmentsThatOverlap(appointment.getStartDate(),appointment.getEndDate(),appointment.getEmployee().getId()).size() > 0) {
            throw new ConflictException(TIMEFRAME_ALREADY_TAKEN);
        }

        var outsideOfWorkingStartHours =
        (appointment.getStartDate().getHour() < activityService.getActivityById(updateDTO.activityId()).getProvider().getStartTimeOfWorkingDays().getHour()) ||

        (appointment.getStartDate().getHour() == activityService.getActivityById(updateDTO.activityId()).getProvider().getStartTimeOfWorkingDays().getHour() &&
        appointment.getStartDate().getMinute() < activityService.getActivityById(updateDTO.activityId()).getProvider().getStartTimeOfWorkingDays().getMinute());

        if (outsideOfWorkingStartHours) {
            throw new ConflictException(OUTSIDE_OF_WORKING_HOURS);
        }

        var updatedAppointment = appointmentRepository.save(appointment);

        sendAppointmentUpdateMail(
                updatedAppointment.getEmployee().getAccount().getEmail(),
                updatedAppointment.getStartDate()
        );

        sendAppointmentUpdateMail(
                updatedAppointment.getClient().getAccount().getEmail(),
                updatedAppointment.getStartDate()
        );

        return appointmentMapper.toResponseDto(updatedAppointment);
    }

    @Override
    public AppointmentResponseDTO getById(Long id) {
       var optionalAppointment = appointmentRepository.findById(id);
       if (optionalAppointment.isEmpty()) {
           throw new NotFoundException(APPOINTMENT_NOT_EXISTS.formatted(id));
       }
       return appointmentMapper.toResponseDto(optionalAppointment.get());
    }

    @Override
    public Optional<Appointment> findById(Long id) {
        return appointmentRepository.findById(id);
    }

    @Override
    public Page<AppointmentResponseDTO> getAll(Pageable pageable, AppointmentSpecification specification) {
        var appointments = appointmentRepository.findAll(specification, pageable);
        return appointments.map(appointmentMapper::toResponseDto);
    }

    private Appointment getAppointmentById(Long id) {
        var optionalAppointment = appointmentRepository.findById(id);
        if (optionalAppointment.isEmpty()) {
            throw new NotFoundException(APPOINTMENT_NOT_EXISTS.formatted(id));
        }
        return optionalAppointment.get();
    }

    @Override
    public List<Appointment> getAppointmentsByActivityId(Long activityId) {
        return appointmentRepository.findAllByActivityId(activityId);
    }

    @Override
    public void delete(Long id) {
       if (!appointmentRepository.existsById(id)) {
           throw new NotFoundException(APPOINTMENT_NOT_EXISTS.formatted(id));
       }
       appointmentRepository.deleteById(id);
    }

    @Override
    @Transactional
    public AppointmentsForClientResponseDto scheduledAppointmentForClients(AppointmentsForClientRequestDto dto,
                                                                           Long clientAccountId) {

        var activities = activityService.getAllByProviderIdAndActivityIds(dto.providerId(), dto.activityIds());

        var employees = employeeService.getByIdsAndProviderId(dto.employeeIds(), dto.providerId());

        if (activities.size() != dto.activityIds().size()) {
            throw new BadRequestException("Some of the specified activities are not for this provider!");
        }

        if (activities.size() != employees.size()) {
            throw new BadRequestException("Number of activities not match the number of employees");
        }

        var client = clientService.getClientByAccountId(clientAccountId);

        double totalPrice = 0;
        var startDate = dto.startDate();

        for (int i = 0; i < activities.size(); i++) {
            var activity = activities.get(i);
            var employee = employees.get(i);

            totalPrice += activity.getPrice();

            Appointment appointment = new Appointment();

            appointment.setStartDate(startDate);
            appointment.setEndDate(startDate.plusNanos(activity.getDuration().toNanos()));
            appointment.setActivity(activity);
            appointment.setClient(client);
            appointment.setEmployee(employee);

            startDate = appointment.getEndDate();

            if (!appointmentRepository.getAllAppointmentsThatOverlap(appointment.getStartDate(),appointment.getEndDate(),employee.getId()).isEmpty()) {
                throw new ConflictException(TIMEFRAME_ALREADY_TAKEN);
            }

            var outsideOfWorkingStartHours =
            (appointment.getStartDate().getHour() < activityService.getActivityById(activity.getId()).getProvider().getStartTimeOfWorkingDays().getHour()) ||

            (appointment.getStartDate().getHour() == activityService.getActivityById(activity.getId()).getProvider().getStartTimeOfWorkingDays().getHour() &&
            appointment.getStartDate().getMinute() < activityService.getActivityById(activity.getId()).getProvider().getStartTimeOfWorkingDays().getMinute());

            if (outsideOfWorkingStartHours) {
                throw new ConflictException(OUTSIDE_OF_WORKING_HOURS);
            }

            var outsideOfWorkingEndHours = (appointment.getEndDate().getHour() > activityService.getActivityById(activity.getId()).getProvider().getEndTimeOfWorkingDays().getHour()) ||
            (appointment.getEndDate().getHour() == activityService.getActivityById(activity.getId()).getProvider().getEndTimeOfWorkingDays().getHour() &&
            appointment.getEndDate().getMinute() > activityService.getActivityById(activity.getId()).getProvider().getEndTimeOfWorkingDays().getMinute());

            if (outsideOfWorkingEndHours)  {
                throw new ConflictException(OUTSIDE_OF_WORKING_HOURS);
            }

            appointmentRepository.save(appointment);

        }
        return new AppointmentsForClientResponseDto(totalPrice);
    }

    private void sendAppointmentMail(String email, LocalDateTime startDate) {
        var dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy.MM.dd 'at' hh:mma");

        emailSender.sendTextMessage(email,
                "Appointment",
                "You have been added to the appointment that starts on: %s".formatted(startDate.format(dateTimeFormatter)));
    }

    private void sendAppointmentUpdateMail(String email, LocalDateTime startDate) {
        var dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy.MM.dd 'at' hh:mma");

        emailSender.sendTextMessage(email,
                "Updated Appointment",
                "You have been updated the appointment that starts on: %s".formatted(startDate.format(dateTimeFormatter)));
    }

    public ActivityService getActivityService() {
        return activityService;
    }

    @Autowired
    @Lazy
    public void setActivityService(ActivityService activityService) {
        this.activityService = activityService;
    }
}
