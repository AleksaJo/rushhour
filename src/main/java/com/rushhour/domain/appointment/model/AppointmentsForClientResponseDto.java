package com.rushhour.domain.appointment.model;

public record AppointmentsForClientResponseDto(
        double totalPrice
) {}
