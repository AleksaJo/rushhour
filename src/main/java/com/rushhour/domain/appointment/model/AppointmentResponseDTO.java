package com.rushhour.domain.appointment.model;

import com.rushhour.domain.activity.model.ActivityResponseDTO;
import com.rushhour.domain.client.model.ClientResponseDTO;
import com.rushhour.domain.employee.model.EmployeeResponseDTO;

import java.time.LocalDateTime;

public record AppointmentResponseDTO(

        Long id,

        LocalDateTime startDate,

        LocalDateTime endDate,

        EmployeeResponseDTO employee,

        ClientResponseDTO client,

        ActivityResponseDTO activity

) {}
