package com.rushhour.domain.appointment.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import java.time.LocalDateTime;

public record AppointmentRequestDTO(

        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        LocalDateTime startDate,
        @Valid
        Long activityId,
        @Valid
        Long employeeId,
        @Valid
        Long clientId

) {}
