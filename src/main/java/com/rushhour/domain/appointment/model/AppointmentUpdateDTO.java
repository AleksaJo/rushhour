package com.rushhour.domain.appointment.model;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

public record AppointmentUpdateDTO(
        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        LocalDateTime startDate,
        Long activityId
) {}
