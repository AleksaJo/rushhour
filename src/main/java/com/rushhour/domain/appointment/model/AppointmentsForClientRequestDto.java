package com.rushhour.domain.appointment.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

public record AppointmentsForClientRequestDto(

        @NotNull(message = "")
        Long providerId,

        @NotEmpty(message = "")
        List<Long> activityIds,

        @NotEmpty(message = "")
        List<Long> employeeIds,

        @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm")
        LocalDateTime startDate

) {}
